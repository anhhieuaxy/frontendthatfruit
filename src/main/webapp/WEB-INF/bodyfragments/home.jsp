<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="resources/js/ajax/ajax_home.js"></script>
<!-- main -->
<main>
    <section id="slider">
        <div class="container-fluid">
            <div class="row">
                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                    <div class="download">
                        <a href="#" class="download-googlePlay">
                            <div class="d__wapper d-flex justify-content-center align-items-center">
                                <i class="fab fa-google-play"></i>
                                <div class="dl__wapper">
                                    <span>Tải về trên</span> <br>
                                    <strong>Google Store</strong>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="download-appStore">
                            <div class="d__wapper d-flex justify-content-center align-items-center">
                                <i class="fab fa-apple"></i>
                                <div class="dl__wapper">
                                    <span>Tải về trên</span> <br>
                                    <strong>App Store</strong>
                                </div>
                            </div>
                        </a>
                    </div>
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleCaptions" data-slide-to="0" class=""></li>
                        <li data-target="#carouselExampleCaptions" data-slide-to="1" class="active"></li>
                        <li data-target="#carouselExampleCaptions" data-slide-to="2" class=""></li>
                        <li data-target="#carouselExampleCaptions" data-slide-to="3" class=""></li>
                        <li data-target="#carouselExampleCaptions" data-slide-to="4" class=""></li>
                        <li data-target="#carouselExampleCaptions" data-slide-to="5" class=""></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item">
                            <img class="d-block w-100" src="resources/img/slider1.png" alt="First slide [800x400]">
                            <div class="carousel-caption d-md-block">
                                <h5>Giao tận tay người dùng</h5>
                                <h2>Thực phẩm Organic</h2>
                                <p>Chúng tôi cung cấp các sản phẩm hữu cơ chất lượng cao</p>
                                <a href="aboutme">Xem chi tiết</a>
                            </div>
                        </div>
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="resources/img/slider2.png" alt="Second slide [800x400]">
                            <div class="carousel-caption d-md-block">
                                <h5>Giao tận tay người dùng</h5>
                                <h2>Thực phẩm Organic</h2>
                                <p>Chúng tôi cung cấp các sản phẩm hữu cơ chất lượng cao</p>
                                <a href="aboutme">Xem chi tiết</a>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="resources/img/slider3.png" alt="Third slide [800x400]">
                            <div class="carousel-caption d-md-block">
                                <h5>Giao tận tay người dùng</h5>
                                <h2>Thực phẩm Organic</h2>
                                <p>Chúng tôi cung cấp các sản phẩm hữu cơ chất lượng cao</p>
                                <a href="aboutme">Xem chi tiết</a>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="resources/img/slider1.png" alt="First slide [800x400]">
                            <div class="carousel-caption d-md-block">
                                <h5>Giao tận tay người dùng</h5>
                                <h2>Thực phẩm Organic</h2>
                                <p>Chúng tôi cung cấp các sản phẩm hữu cơ chất lượng cao</p>
                                <a href="aboutme">Xem chi tiết</a>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="resources/img/slider1.png" alt="First slide [800x400]">
                            <div class="carousel-caption d-md-block">
                                <h5>Giao tận tay người dùng</h5>
                                <h2>Thực phẩm Organic</h2>
                                <p>Chúng tôi cung cấp các sản phẩm hữu cơ chất lượng cao</p>
                                <a href="aboutme">Xem chi tiết</a>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="resources/img/slider1.png" alt="First slide [800x400]">
                            <div class="carousel-caption d-md-block">
                                <h5>Giao tận tay người dùng</h5>
                                <h2>Thực phẩm Organic</h2>
                                <p>Chúng tôi cung cấp các sản phẩm hữu cơ chất lượng cao</p>
                                <a href="aboutme">Xem chi tiết</a>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon carousel-control-next-icon-home" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                        <span class="carousel-control-next-icon carousel-control-next-icon-home" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section id="productmenu">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="productmenu__wapper">
                        <div class="wappert">
                            <div class="wptx">
                                <div class="productmenu__selection ps1">
                                    <img src="resources/img/775A8390.jpg" alt="">
                                </div>
                                <span>Ưu đãi đặc biệt</span>
                            </div>
                        </div>
                        <div class="wappert">
                            <div class="wptx">
                                <div class="productmenu__selection ps2">
                                    <img src="resources/img/775A8660.jpg" alt="">
                                </div>
                                <span>Sản phẩm bán chạy</span>
                            </div>
                        </div>
                        <div class="wappert">
                           <div class="wptx">
                               <div class="productmenu__selection ps3">
                                   <img src="resources/img/775A8581.jpg" alt="">
                               </div>
                               <span>Sản phẩm nổi bật</span>
                           </div>
                        </div>
                        <div class="wappert">
                           <div class="wptx">
                               <div class="productmenu__selection ps4">
                                   <img src="resources/img/775A8583.jpg" alt="">
                               </div>
                               <span>Ưu đãi của tháng</span>
                           </div>
                        </div>
                        <div class="wappert">
                           <div class="wptx">
                               <div class="productmenu__selection ps5">
                                   <img src="resources/img/775A8668.jpg" alt="">
                               </div>
                               <span>Tin Tức</span>
                           </div>
                        </div>
                        <div class="wappert">
                           <div class="wptx">
                               <div class="productmenu__selection ps6">
                                   <img src="resources/img/775A8610 copy.jpg" alt="">
                               </div>
                               <span>Chúng Tôi</span>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <hr class="divider">
    <section id="service">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="service__wapper d-flex align-items-center ">
                        <i class="fas fa-car"></i>
                        <div class="service__text">
                            <p class="m-0">Miễn phí vận chuyển</p>
                            <span>Áp dụng cho đơn hàng từ 500000 trở lên</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="service__wapper d-flex align-items-center ">
                        <i class="fas fa-sync"></i>
                        <div class="service__text">
                            <p class="m-0">Hoàn tiền miễn phí</p>
                            <span>Hoàn tiền 100% trong 3 ngày đầu tiên</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="service__wapper d-flex align-items-center ">
                        <i class="fas fa-phone-volume"></i>
                        <div class="service__text">
                            <p class="m-0">Hỗ trợ 24/7</p>
                            <span>Gọi cho chúng tôi bất cứ khi nào bạn muốn</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="supperoffer">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h3>Ưu đãi đặc biệt</h3>
                </div>
                <div class="col-4">
                    <div class="wapper so-wapper">
                        <div class="sot-wapper">
                            <p>Chanh không hạt</p>
                            <span>128.000 VNĐ/1Kg</span>
                            <button class="btn" type="button"> Xem ngay
                                <i class="fas fa-long-arrow-alt-right"></i>
                            </button>
                        </div>
                        <div class="supperoffer__selection of1">
                        </div>
                    </div>
                </div>
                <div class="col-4 d-flex flex-column  justify-content-between">
                    <div class="wapper so-wapper1">
                        <div class="sot-wapper ef2">
                            <p>Up to 70% Off</p>
                            <button class="btn" type="button"> Xem ngay
                                <i class="fas fa-long-arrow-alt-right"></i>
                            </button>
                        </div>
                        <div class="supperoffer__selection of2">
                        </div>

                    </div>
                    <div class="wapper so-wapper1">
                        <div class="sot-wapper ef3">
                            <p>Sale 50% Off</p>
                            <button class="btn" type="button"> Xem ngay
                                <i class="fas fa-long-arrow-alt-right"></i>
                            </button>
                        </div>
                        <div class="supperoffer__selection of3">
                        </div>

                    </div>
                </div>
                <div class="col-4">
                    <div class="wapper so-wapper">
                        <div class="sot-wapper">
                            <p>Dâu tây</p>
                            <span>128.000 VNĐ/1Kg</span>
                            <button class="btn" type="button"> Xem ngay
                                <i class="fas fa-long-arrow-alt-right"></i>
                            </button>
                        </div>
                        <div class="supperoffer__selection of4">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="bestsellers">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="text-center">Sản phẩm bán chạy</h3>
                </div>
                <div class="col-12">
                    <ul class="bs-list">
                        <li><a href="#">Tất cả</a></li>
                        <li><a href="#">Trái cây tươi</a></li>
                        <li><a href="#">Trái cây khô</a></li>
                        <li><a href="#">Nước hoa quả</a></li>
                    </ul>
                </div>
                <div class="col-12">
                    <div id="carousel3Column" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row d-flex justify-content-center">
                                    <div class="p-1">
                                        <div class="card">
                                            <div class="card-img">
                                                <img src="resources/img/grape.png" >
                                                <i class="fas fa-eye"></i>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title text-center">Nho Mĩ</h5>
                                                <p class="card-text text-center">345.000 đ</p>
                                                <div class="card__wp">
                                                    <i class="far fa-heart" data-toggle="tooltip" title="Wishlist!" data-placement="bottom"></i>
                                                    <a  class="btn btn-outline-success w-100">Thêm vào giỏ</a>
                                                    <i class="far fa-gem" data-toggle="tooltip" title="Compare"
                                                       data-placement="bottom"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class="card">
                                            <div class="card-img">
                                                <img src="resources/img/strawberry4.png" >
                                                <i class="fas fa-eye"></i>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title text-center">Dâu tây Đà Lạt</h5>
                                                <p class="card-text text-center">125.000 đ</p>
                                                <div class="card__wp">
                                                    <i class="far fa-heart" data-toggle="tooltip" title="Wishlist!" data-placement="bottom"></i>
                                                    <a href="#" class="btn btn-outline-success w-100">Thêm vào giỏ</a>
                                                    <i class="far fa-gem" data-toggle="tooltip" title="Compare"
                                                       data-placement="bottom"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class="card">
                                            <div class="card-img">
                                                <img src="resources/img/lemon.png" >
                                                <i class="fas fa-eye"></i>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title text-center">Chanh không hạt</h5>
                                                <p class="card-text text-center">145.000 đ</p>
                                                <div class="card__wp">
                                                    <i class="far fa-heart" data-toggle="tooltip" title="Wishlist!" data-placement="bottom"></i>
                                                    <a href="#" class="btn btn-outline-success w-100">Thêm vào giỏ</a>
                                                    <i class="far fa-gem" data-toggle="tooltip" title="Compare"
                                                       data-placement="bottom"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item ">
                                <div class="row d-flex justify-content-center">
                                    <div class="p-1">
                                        <div class="card">
                                            <div class="card-img">
                                                <img src="resources/img/grape.png" class="">
                                                <i class="fas fa-eye"></i>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title text-center">Nho Mĩ</h5>
                                                <p class="card-text text-center">345.000 đ</p>
                                                <div class="card__wp">
                                                    <i class="far fa-heart" data-toggle="tooltip" title="Wishlist!" data-placement="bottom"></i>
                                                    <a href="#" class="btn btn-outline-success w-100">Thêm vào giỏ</a>
                                                    <i class="far fa-gem" data-toggle="tooltip" title="Compare"
                                                       data-placement="bottom"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class="card">
                                            <div class="card-img">
                                                <img src="resources/img/strawberry4.png" class="">
                                                <i class="fas fa-eye"></i>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title text-center">Dâu tây Đà Lạt</h5>
                                                <p class="card-text text-center">125.000 đ</p>
                                                <div class="card__wp">
                                                    <i class="far fa-heart" data-toggle="tooltip" title="Wishlist!" data-placement="bottom"></i>
                                                    <a href="#" class="btn btn-outline-success w-100">Thêm vào giỏ</a>
                                                    <i class="far fa-gem" data-toggle="tooltip" title="Compare"
                                                       data-placement="bottom"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class="card">
                                            <div class="card-img">
                                                <img src="resources/img/lemon.png" class="">
                                                <i class="fas fa-eye"></i>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title text-center">Chanh không hạt</h5>
                                                <p class="card-text text-center">145.000 đ</p>
                                                <div class="card__wp">
                                                    <i class="far fa-heart" data-toggle="tooltip" title="Wishlist!" data-placement="bottom"></i>
                                                    <a href="#" class="btn btn-outline-success w-100">Thêm vào giỏ</a>
                                                    <i class="far fa-gem" data-toggle="tooltip" title="Compare"
                                                       data-placement="bottom"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carousel3Column" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"><i class="fas fa-long-arrow-alt-left"></i></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel3Column" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"><i class="fas fa-long-arrow-alt-right"></i></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="featuredsproducts">
        <div class="container">
            <div class="row blog">
                <div class="col-12">
                    <h3 class="text-center">Sản phẩm nổi bật</h3>
                </div>
                <div class="col-md-12">
                    <div id="blogCarousel" class="carousel slide" data-ride="carousel">
                        <!-- <ol class="carousel-indicators">
                                          <li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
                                          <li data-target="#blogCarousel" data-slide-to="1"></li>
                                      </ol> -->
                        <!-- Carousel items -->
                        <div class="carousel-inner" id="cr">
                            <div class="carousel-item active">
                                <div class="row">
                                    <div class="col-5">
                                        <div class="flip-box">
                                            <div class="flip-box-inner">
                                                <div class="flip-box-front">
                                                    <img src="resources/img/strawberry2.png" alt="">
                                                </div>
                                                <div class="flip-box-back">
                                                    <img src="resources/img/strawberry3.png">
                                                    <i class="fas fa-eye"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-7">
                                        <div class="fp__wapper">
                                            <span class="fp__cap">Dâu tây Đà Lạt</span>
                                            <i class="fp__price"><del>200.000 đ</del> 125.000 đ</i>
                                            <div class="rating">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                            </div>
                                            <hr class="divider">
                                            <p>Dâu tây Đà Lạt giống New Zealand là một trong các giống dâu tây cao cấp được trồng thủy canh
                                                tại Đà
                                                Lạt. Trái dâu tây Đà Lạt giống New Zealand có màu sắc bắt mắt, thịt giòn, ngọt và đặc biệt là
                                                có hương
                                                thơm đặc trưng không thua kém gì dâu tây nhập khẩu.</p>
                                            <a href="#" class="fp__label"> Thêm vào giỏ</a>
                                            <i class="far fa-heart" data-toggle="tooltip" title="Wishlist!" data-placement="bottom"></i>
                                            <i class="far fa-gem" data-toggle="tooltip" title="Compare"
                                               data-placement="bottom"></i>
                                        </div>
                                    </div>
                                    <!--.row-->
                                </div>
                                <!--.item-->
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-5">
                                        <div class="flip-box">
                                            <div class="flip-box-inner">
                                                <div class="flip-box-front">
                                                    <img src="resources/img/strawberry2.png" alt="">

                                                </div>
                                                <div class="flip-box-back">
                                                    <img src="resources/img/strawberry3.png">
                                                    <i class="fas fa-eye"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-7">
                                        <div class="fp__wapper">
                                            <span class="fp__cap">Dâu tây Đà Lạt</span>
                                            <i class="fp__price"><del>200.000 đ</del> 125.000 đ</i>
                                            <div class="rating">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                            </div>
                                            <hr class="divider">
                                            <p>Dâu tây Đà Lạt giống New Zealand là một trong các giống dâu tây cao cấp được trồng thủy canh
                                                tại Đà
                                                Lạt. Trái dâu tây Đà Lạt giống New Zealand có màu sắc bắt mắt, thịt giòn, ngọt và đặc biệt là
                                                có hương
                                                thơm đặc trưng không thua kém gì dâu tây nhập khẩu.</p>
                                            <a href="#" class="fp__label"> Thêm vào giỏ</a>
                                            <i class="far fa-heart" data-toggle="tooltip" title="Wishlist!" data-placement="bottom"></i>
                                            <i class="far fa-gem" data-toggle="tooltip" title="Compare"
                                               data-placement="bottom"></i>
                                        </div>
                                        <!--.row-->
                                    </div>
                                    <!--.item-->
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-5">
                                        <div class="flip-box">
                                            <div class="flip-box-inner">
                                                <div class="flip-box-front">
                                                    <img src="resources/img/strawberry2.png" alt="">

                                                </div>
                                                <div class="flip-box-back">
                                                    <img src="resources/img/strawberry3.png">
                                                    <i class="fas fa-eye"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-7">
                                        <div class="fp__wapper">
                                            <span class="fp__cap">Dâu tây Đà Lạt</span>
                                            <i class="fp__price"><del>200.000 đ</del> 125.000 đ</i>
                                            <div class="rating">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                            </div>
                                            <hr class="divider">
                                            <p>Dâu tây Đà Lạt giống New Zealand là một trong các giống dâu tây cao cấp được trồng thủy canh
                                                tại Đà
                                                Lạt. Trái dâu tây Đà Lạt giống New Zealand có màu sắc bắt mắt, thịt giòn, ngọt và đặc biệt là
                                                có hương
                                                thơm đặc trưng không thua kém gì dâu tây nhập khẩu.</p>
                                            <a href="#" class="fp__label"> Thêm vào giỏ</a>
                                            <i class="far fa-heart" data-toggle="tooltip" title="Wishlist!" data-placement="bottom"></i>
                                            <i class="far fa-gem" data-toggle="tooltip" title="Compare"
                                               data-placement="bottom"></i>
                                        </div>
                                        <!--.row-->
                                    </div>
                                    <!--.item-->
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-5">
                                        <div class="flip-box">
                                            <div class="flip-box-inner">
                                                <div class="flip-box-front">
                                                    <img src="resources/img/strawberry2.png" alt="">

                                                </div>
                                                <div class="flip-box-back">
                                                    <img src="resources/img/strawberry3.png">
                                                    <i class="fas fa-eye"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-7">
                                        <div class="fp__wapper">
                                            <span class="fp__cap">Dâu tây Đà Lạt</span>
                                            <i class="fp__price"><del>200.000 đ</del> 125.000 đ</i>
                                            <div class="rating">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                            </div>
                                            <hr class="divider">
                                            <p>Dâu tây Đà Lạt giống New Zealand là một trong các giống dâu tây cao cấp được trồng thủy canh
                                                tại Đà
                                                Lạt. Trái dâu tây Đà Lạt giống New Zealand có màu sắc bắt mắt, thịt giòn, ngọt và đặc biệt là
                                                có hương
                                                thơm đặc trưng không thua kém gì dâu tây nhập khẩu.</p>
                                            <a href="#" class="fp__label"> Thêm vào giỏ</a>
                                            <i class="far fa-heart" data-toggle="tooltip" title="Wishlist!" data-placement="bottom"></i>
                                            <i class="far fa-gem" data-toggle="tooltip" title="Compare"
                                               data-placement="bottom"></i>
                                        </div>
                                        <!--.row-->
                                    </div>
                                    <!--.item-->
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-5">
                                        <div class="flip-box">
                                            <div class="flip-box-inner">
                                                <div class="flip-box-front">
                                                    <img src="resources/img/strawberry2.png" alt="">

                                                </div>
                                                <div class="flip-box-back">
                                                    <img src="resources/img/strawberry3.png">
                                                    <i class="fas fa-eye"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-7">
                                        <div class="fp__wapper">
                                            <span class="fp__cap">Dâu tây Đà Lạt</span>
                                            <i class="fp__price"><del>200.000 đ</del> 125.000 đ</i>
                                            <div class="rating">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                            </div>
                                            <hr class="divider">
                                            <p>Dâu tây Đà Lạt giống New Zealand là một trong các giống dâu tây cao cấp được trồng thủy canh
                                                tại Đà
                                                Lạt. Trái dâu tây Đà Lạt giống New Zealand có màu sắc bắt mắt, thịt giòn, ngọt và đặc biệt là
                                                có hương
                                                thơm đặc trưng không thua kém gì dâu tây nhập khẩu.</p>
                                            <a href="#" class="fp__label"> Thêm vào giỏ</a>
                                            <i class="far fa-heart" data-toggle="tooltip" title="Wishlist!" data-placement="bottom"></i>
                                            <i class="far fa-gem" data-toggle="tooltip" title="Compare"
                                               data-placement="bottom"></i>
                                        </div>
                                        <!--.row-->
                                    </div>
                                    <!--.item-->
                                </div>
                            </div>
                            <!--.carousel-inner-->
                        </div>
                        <a class="carousel-control-prev" href="#blogCarousel" data-slide="prev">
                            <span class="carousel-control-prev-icon"><i class="fas fa-long-arrow-alt-left"></i></span>
                        </a>
                        <a class="carousel-control-next" href="#blogCarousel" data-slide="next">
                            <span class="carousel-control-next-icon"><i class="fas fa-long-arrow-alt-right"></i></span>
                        </a>
                        <!--.Carousel-->

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="dealsmonth">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="text-center">Ưu đãi của tháng</h3>
                </div>
                <div class="col-12">
                    <div id="carousel3Column1" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row d-flex justify-content-center">
                                    <div class="p-1">
                                        <div class="card">
                                            <a href="#" class="count">623 : 2 : 32 : 20</a>
                                            <div class="card-img">
                                                <img src="resources/img/grape.png" >
                                                <i class="fas fa-eye"></i>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title text-center">Nho Mĩ</h5>
                                                <p class="card-text text-center"> <del>400.000d</del> 345.000 đ <span class="sale">-25%</span></p>
                                                <div class="card__wp">
                                                    <i class="far fa-heart" data-toggle="tooltip" title="Wishlist!" data-placement="bottom"></i>
                                                    <a href="#" class="btn btn-outline-success w-100">Thêm vào giỏ</a>
                                                    <i class="far fa-gem" data-toggle="tooltip" title="Compare"
                                                       data-placement="bottom"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class="card">
                                            <a href="#" class="count">623 : 2 : 32 : 20</a>
                                            <div class="card-img">
                                                <img src="resources/img/strawberry4.png" >
                                                <i class="fas fa-eye"></i>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title text-center">Dâu tây Đà Lạt</h5>
                                                <p class="card-text text-center"> <del>400.000d</del> 125.000 đ <span class="sale">-25%</span></p>
                                                <div class="card__wp">
                                                    <i class="far fa-heart" data-toggle="tooltip" title="Wishlist!" data-placement="bottom"></i>
                                                    <a href="#" class="btn btn-outline-success w-100">Thêm vào giỏ</a>
                                                    <i class="far fa-gem" data-toggle="tooltip" title="Compare"
                                                       data-placement="bottom"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class="card">
                                            <a href="#" class="count">623 : 2 : 32 : 20</a>
                                            <div class="card-img">
                                                <img src="resources/img/lemon.png" >
                                                <i class="fas fa-eye"></i>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title text-center">Chanh không hạt</h5>
                                                <p class="card-text text-center"><del>400.000d</del> 145.000 đ <span class="sale">-25%</span></p>
                                                <div class="card__wp">
                                                    <i class="far fa-heart" data-toggle="tooltip" title="Wishlist!" data-placement="bottom"></i>
                                                    <a href="#" class="btn btn-outline-success w-100">Thêm vào giỏ</a>
                                                    <i class="far fa-gem" data-toggle="tooltip" title="Compare"
                                                       data-placement="bottom"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row d-flex justify-content-center">
                                    <div class="p-1">
                                        <div class="card">
                                            <a href="#" class="count">623 : 2 : 32 : 20</a>
                                            <div class="card-img">
                                                <img src="resources/img/grape.png" >
                                                <i class="fas fa-eye"></i>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title text-center">Nho Mĩ</h5>
                                                <p class="card-text text-center"><del>400.000d</del>  345.000 đ <span class="sale">-25%</span></p>
                                                <div class="card__wp">
                                                    <i class="far fa-heart" data-toggle="tooltip" title="Wishlist!" data-placement="bottom"></i>
                                                    <a href="#" class="btn btn-outline-success w-100">Thêm vào giỏ</a>
                                                    <i class="far fa-gem" data-toggle="tooltip" title="Compare"
                                                       data-placement="bottom"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class="card">
                                            <a href="#" class="count">623 : 2 : 32 : 20</a>
                                            <div class="card-img">
                                                <img src="resources/img/strawberry4.png" >
                                                <i class="fas fa-eye"></i>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title text-center">Dâu tây Đà Lạt</h5>
                                                <p class="card-text text-center"><del>400.000d</del>  125.000 đ <span class="sale">-25%</span></p>
                                                <div class="card__wp">
                                                    <i class="far fa-heart" data-toggle="tooltip" title="Wishlist!" data-placement="bottom"></i>
                                                    <a href="#" class="btn btn-outline-success w-100">Thêm vào giỏ</a>
                                                    <i class="far fa-gem" data-toggle="tooltip" title="Compare"
                                                       data-placement="bottom"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class="card">
                                            <a href="#" class="count">623 : 2 : 32 : 20</a>
                                            <div class="card-img">
                                                <img src="resources/img/lemon.png" >
                                                <i class="fas fa-eye"></i>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title text-center">Chanh không hạt</h5>
                                                <p class="card-text text-center"><del>400.000d</del>  145.000 đ <span class="sale">-25%</span></p>
                                                <div class="card__wp">
                                                    <i class="far fa-heart" data-toggle="tooltip" title="Wishlist!" data-placement="bottom"></i>
                                                    <a href="#" class="btn btn-outline-success w-100">Thêm vào giỏ</a>
                                                    <i class="far fa-gem" data-toggle="tooltip" title="Compare"
                                                       data-placement="bottom"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <a class="carousel-control-prev" href="#carousel3Column1" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"><i class="fas fa-long-arrow-alt-left"></i></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel3Column1" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"><i class="fas fa-long-arrow-alt-right"></i></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="news">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="text-center">Tin tức</h3>
                </div>
                <div class="col-6">
                    <img href src="resources/img/salad1.png" alt="">
                    <a href="#" class="label1">Fruits Fresh</a>
                    <span class="newscap">Có nên ăn salad cho bữa sáng không</span>
                    <time class="">by <strong>7UP Theme</strong> <span>Aprial 27,2019</span></time>
                    <p>Mặc dù ăn rau cho bữa sáng không phải là phổ biến trong chế độ ăn kiêng phương Tây, nhưng nó khá phổ biến
                        trong chế độ ăn kiêng từ các nơi khác trên thế giới.</p>
                </div>
                <div class="col-6">
                    <a href="#" class="label1 ">Fruits Fresh</a>
                    <span class="newscap">Có nên ăn Salad cho bữa sáng không?</span>
                    <time class="">by <strong>7UP Theme</strong> <span>Aprial 27,2019</span></time>
                    <p>Mặc dù ăn rau cho bữa sáng không phải là phổ biến trong chế độ ăn kiêng phương Tây, nhưng nó khá phổ biến
                        trong chế độ ăn kiêng từ các nơi khác trên thế giới.</p>
                    <img src="resources/img/salad2.png" alt="">
                </div>
                <div class="col-12 d-flex justify-content-center">
                    <button class="btn" type="button"> Xem tất cả
                        <i class="fas fa-long-arrow-alt-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </section>
    <section id="feedback">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="text-center">Mọi người nói về chúng tôi</h3>
                    <div class="fb__wapper">
                        <div class="img__wapper">
                            <img src="resources/img/user1.png" alt="">
                        </div>
                        <div class="img__wapper active1">
                            <img src="resources/img/user2.png" alt="">
                        </div>
                        <div class="img__wapper">
                            <img src="resources/img/user3.png" alt="">
                        </div>
                    </div>
                    <p class="text-center">Hoa quả của shop tươi ngon đúng như tên gọi Thật fruit. Mình mua online mà cảm thấy rất ưng ý! Bán hàng có tâm lắm nhé!.</p>
                    <span class="text-center d-block fb-name">Thật Fruit</span>
                    <span class="text-center d-block">CEO/Founder</span>
                </div>
            </div>
        </div>
    </section>
</main>
<!-- end main -->

<!-- partner -->
<section id="partner">
    <div class="container">
        <div class="row blog">
            <div class="col-md-12">
                <div id="blogCarouselb" class="carousel slide" data-ride="carousel">
                    <!-- <ol class="carousel-indicators">
                                  <li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
                                  <li data-target="#blogCarousel" data-slide-to="1"></li>
                              </ol> -->
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-md-3 col-6">
                                    <a class="">
                                        <img src="resources/img/sendobigimg.png" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                <div class="col-md-3 col-6">
                                    <a >
                                        <img src="resources/img/fear.png" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                <div class="col-md-3 col-6">
                                    <a >
                                        <img src="resources/img/spark.png" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                <div class="col-md-3 col-6">
                                    <a >
                                        <img src="resources/img/be.png" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                </div>
                            </div>
                            <!--.row-->
                        <!--.item-->
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-3 col-6 ">
                                    <a href="#">
                                        <img src="resources/img/sendobigimg.png" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                <div class="col-md-3 col-6">
                                    <a href="#">
                                        <img src="resources/img/fear.png" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                <div class="col-md-3 col-6">
                                    <a href="#">
                                        <img src="resources/img/spark.png" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                <div class="col-md-3 col-6">
                                    <a href="#">
                                        <img src="resources/img/be.png" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                <!--.row-->
                            </div>
                            <!--.item-->

                        </div>
                        <!--.item-->
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-3 col-6 ">
                                    <a href="#">
                                        <img src="resources/img/sendobigimg.png" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                <div class="col-md-3 col-6">
                                    <a href="#">
                                        <img src="resources/img/fear.png" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                <div class="col-md-3 col-6">
                                    <a href="#">
                                        <img src="resources/img/spark.png" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                <div class="col-md-3 col-6">
                                    <a href="#">
                                        <img src="resources/img/be.png" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                <!--.row-->
                            </div>
                            <!--.item-->

                        </div>
                        <!--.carousel-inner-->
                    </div>
                    <!--.Carousel-->

                </div>
            </div>
        </div>
    </div>
</section>
<!-- end partner -->
<!-- mail area -->
<section id="mail-area">
    <div class="container">
        <div class="row d-flex align-items-center justify-content-around">
            <div class="col-md-3 col-sm-12">
                <p class="text-center m-0">Đăng kí để nhận lời khuyên, tin tức và khuyến mãi từ Thật Fruit</p>
            </div>
            <div class="col-md-5 col-sm-12">
                <div class="input-group">
                    <input type="text" class="  search-query form-control" placeholder="Emai của bạn" id="valsmail" />
                    <span class="input-group-btn">
              <button class="btn" type="button" id="btsmail" onclick="setFormFeedBack()">
                Đăng kí
              </button>
            </span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end mail area -->
<!-- map -->
<section id="map" class="map-google">
    <div class="container-fluid">
        <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.427001229873!2d105.84818131431808!3d21.01559399360486!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab8ce525cbaf%3A0x5663ae2fcc524eb7!2zMTAyIFRyaeG7h3UgVmnhu4d0IFbGsMahbmcsIELDuWkgVGjhu4sgWHXDom4sIEhhaSBCw6AgVHLGsG5nLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1565167687603!5m2!1svi!2s"
                width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</section>
<!-- end map -->