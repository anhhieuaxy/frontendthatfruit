<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="resources/js/ajax/ajax_user.js" ></script>
<section id="orderdetail" >
    <div class="order col-lg-7 col-md-9 col-sm-12 col-xs-12">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="orderdetail-wp">
                    <div class="orwp-wp" id="detailPr">

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

<main>
    <section id="banner">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="banner__wp">
                        <div class="banner-text">
                            <h4>Khách hàng</h4>
                            <a href="home">Trang chủ</a>
                            <span>// Khách hàng</span>
                        </div>
                        <div class="banner-bg">
                            <img src="resources/img/banner5.jpg" alt="">
                        </div>
                        <span class="bor bor1"></span>
                        <span class="bor bor2"></span>
                        <span class="bor bor3"></span>
                        <span class="bor bor4"></span>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section id="userinfo">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 mb-3">
                    <div id="input-info-user-cart" class="userinfo-wp">
                        <div class="usif-a">
                            <div class="usifa-img">
                                <img src="resources/img/user_male2-512.png" alt="">
                            </div>
                            <span>Thông tin</span>
                        </div>
                        <div class="usif-if">
                            <div class="us2if-wp">
                                <span>Tên</span>
                                <input type="text">
                            </div>
                            <div class="us2if-wp">
                                <span>Email</span>
                                <input type="text">
                            </div>
                            <div class="us2if-wp">
                                <span>Số điện thoại</span>
                                <input type="text">
                            </div>
                            <div class="us2if-wp">
                                <span>Địa chỉ</span>
                                <input type="text">
                            </div>
                            <div class="us2if-wp">
                                <span>Công việc</span>
                                <input type="text">
                            </div>
                            <div class="us2if-wp">
                                <span>Mô tả</span>
                                <input type="text">
                            </div>

                        </div>
                        <div class="usinu">
                            <button type="submit" class="btn" style="background: var(--color-brown);">Cập nhật</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 mb-3">
                    <div class="tranhis-wp">
                        <span class="tranhis-cap">Lịch sử giao dịch</span>
                        <div class="tranhistb-wp" id="demo2">
                            <table class="table table-hover" >
                                <thead>
                                <tr>
                                    <th scope="col">STT</th>
                                    <th scope="col">Thời gian</th>
                                    <th scope="col">Mã đơn hàng</th>
                                    <th scope="col">Tổng tiền</th>
                                    <th scope="col">Chi Tiết</th>
                                </tr>
                                </thead>
                                <tbody id="tb-buyform">

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- mail area -->
    <section id="mail-area">
        <div class="container">
            <div class="row d-flex align-items-center justify-content-around">
                <div class="col-md-3 col-sm-12">
                    <p class="text-center m-0">Đăng kí để nhận lời khuyên, tin tức và khuyến mãi từ Thật Fruit</p>
                </div>
                <div class="col-md-5 col-sm-12">
                    <div class="input-group">
                        <input type="text" class="  search-query form-control" placeholder="Emai của bạn" id="valsmail" />
                        <span class="input-group-btn">
              <button class="btn" type="button" id="btsmail" onclick="setFormFeedBack()">
                Đăng kí
              </button>
            </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end mail area -->
</main>