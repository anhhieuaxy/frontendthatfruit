<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="resources/js/ajax/ajax_detailProduct.js"></script>
<!-- MAIN -->
<main>
<section class="container">
    <div class="row" id="featuredsproducts">
        <div class="block-detail" id="blockItemDetail">

        </div>
    </div>

</section>
<section class="container">
    <div class="row">

        <div class="block-tab-detail" id="info-prduct-detail">



        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="detail-pr-connection text-uppercase">
            <span>Sản Phẩm Liên Quan</span>
        </div>
        <div class="list-product list-dt">
            <ul class="row" id="rl-prduct">



            </ul>
        </div>
    </div>


</section>
    <section id="mail-area">
        <div class="container">
            <div class="row d-flex align-items-center justify-content-around">
                <div class="col-md-3 col-sm-12">
                    <p class="text-center m-0">Đăng kí để nhận lời khuyên, tin tức và khuyến mãi từ Thật Fruit</p>
                </div>
                <div class="col-md-5 col-sm-12">
                    <div class="input-group">
                        <input type="text" class="  search-query form-control" placeholder="Emai của bạn" id="valsmail" />
                        <span class="input-group-btn">
              <button class="btn" type="button" id="btsmail" onclick="setFormFeedBack()">
                Đăng kí
              </button>
            </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<!-- END MAIN -->
