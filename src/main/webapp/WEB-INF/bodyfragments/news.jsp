<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="resources/js/ajax/ajax_news.js"></script>

<main>
    <section id="banner">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="banner__wp">
                        <div class="banner-text">
                            <h4>Tin tức</h4>
                            <a href="home">Trang chủ</a>
                            <span>// Tin Tức</span>
                        </div>
                        <div class="banner-bg">
                            <img src="resources/img/banner3.jpg" alt="">
                        </div>
                        <span class="bor bor1"></span>
                        <span class="bor bor2"></span>
                        <span class="bor bor3"></span>
                        <span class="bor bor4"></span>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section id="newspage">
        <div class="container" id="pagi-news">
            <div class="row" id='news-products'>

            </div>

        </div>
    </section>
    <!-- mail area -->
    <section id="mail-area">
        <div class="container">
            <div class="row d-flex align-items-center justify-content-around">
                <div class="col-md-3 col-sm-12">
                    <p class="text-center m-0">Đăng kí để nhận lời khuyên, tin tức và khuyến mãi từ Thật Fruit</p>
                </div>
                <div class="col-md-5 col-sm-12">
                    <div class="input-group">
                        <input type="text" class="  search-query form-control" placeholder="Emai của bạn" id="valsmail" />
                        <span class="input-group-btn">
              <button class="btn" type="button" id="btsmail" onclick="setFormFeedBack()">
                Đăng kí
              </button>
            </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end mail area -->
</main>