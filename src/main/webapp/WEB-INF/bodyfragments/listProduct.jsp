<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="resources/js/ajax/ajax_listProduct.js"></script>

<!-- MAIN -->
<!-- MAIN -->
<main>

    <section class="container">
        <div class="row">
            <div class="infor-list-product">
                <div id="tt-list" class="if-list-pr-left">

                </div>

                <div class="if-list-pr-right">
                    <span>Sắp xếp</span>
                    <div class="select-soft">
                        <span>Mặc định <i class="fa fa-caret-down" aria-hidden="true"></i></span>
                        <ul>
                            <li   id="fildef">Mặc định</li>
                            <li  id="filprice">Theo giá</li>
                        </ul>
                    </div>
<%--                    onclick="getProduct()"--%>
<%--                    onclick="priceSoft()"--%>
<%--                    <i id="horizontalList" class="fa fa-list-ul" aria-hidden="true"></i>--%>
<%--                    <i id="gridList" class="fa fa-th" aria-hidden="true"></i>--%>
                </div>
            </div>
            <div class="list-product">

                <div id="featuredsproducts" class="row col-md-12" style="padding: 0">

                    <div class="inner-left preview" id="demo1" style="width: 100%">
                        <div class="fa-prd">

                        </div>
                    </div>

                    <!--.row-->
                </div>
                <!--.item-->


            </div>

        </div>


    </section>
</main>
<!-- END MAIN -->
<!-- END MAIN -->

<!-- mail area -->
<section id="mail-area">
    <div class="container">
        <div class="row d-flex align-items-center justify-content-around">
            <div class="col-md-3 col-sm-12">
                <p class="text-center m-0">Đăng kí để nhận lời khuyên, tin tức và khuyến mãi từ Thật Fruit</p>
            </div>
            <div class="col-md-5 col-sm-12">
                <div class="input-group">
                    <input type="text" class="  search-query form-control" placeholder="Emai của bạn"/>
                    <span class="input-group-btn">
                            <button class="btn" type="button" onclick="setFormFeedBack()">
                                Đăng kí
                            </button>
                        </span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end mail area -->