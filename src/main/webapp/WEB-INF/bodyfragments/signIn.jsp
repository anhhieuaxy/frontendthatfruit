<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="resources/js/ajax/ajax_signIn.js"></script>

<main>
    <section>
        <div class="container">
            <div class="row">
                <div class="block-sign-in ">
                    <div class="form-sign col-lg-4 col-md-6 col-xs-8 " method="POST" role="form">
                        <legend style="text-transform: uppercase;">Đăng Kí Tài Khoản</legend>
                        <div class="form-group">
                            <input id="name" type="text" class="form-control"  placeholder="Họ và tên" required
                                   pattern="">
                            <span class="error-sign">Vui lòng nhập họ và tên.</span>
                        </div>
                        <div class="form-group">

                            <input id="email" type="text" class="form-control"placeholder="Email" required>
                            <span class="error-sign">Vui lòng nhập đúng email</span>
                        </div>
                        <div class="form-group">

                            <input id="password" type="password" class="form-control"  placeholder="Mật khẩu"
                                   required>
                            <span class="error-sign">Vui lòng nhập mật khẩu </span>
                        </div>
                        <div class="form-group">
                            <input id="password-v2" type="password" class="form-control" id=""
                                   placeholder="Nhập lại mật khẩu" required>
                            <span class="error-sign">Vui lòng nhập lại mật khẩu</span>
                        </div>
                        <button id="submitSign" type="submit" class="btn ">Đăng ký</button>
                    </div>
                    <div class="section-or col-lg-4 col-md-6 col-xs-8">
                        <a href="">Hoặc đăng nhập</a>
                    </div>
                    <div class="direc">
                        <div class="form-direc ">
                            <p>Đăng nhập | Tạo tài khoản với</p>
                            <ul>
                                <li>
                                    <a href="">
                                        <img src="https://img.icons8.com/ios-filled/46/3b5998/facebook-new.png">
                                    </a>

                                </li>
                                <li>
                                    <a href="">
                                        <img src="https://img.icons8.com/color/48/000000/google-plus--v2.png">
                                    </a>

                                </li>
                                <li>
                                    <a href="">
                                        <img src="https://img.icons8.com/color/48/000000/twitter-circled.png">
                                    </a>

                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>