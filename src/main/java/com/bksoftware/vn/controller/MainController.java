package com.bksoftware.vn.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.bksoftware.vn.config.SecurityConstants.SECRET;
import static com.bksoftware.vn.config.SecurityConstants.TOKEN_PREFIX;

@Controller
public class MainController {

    private String token;

    public String getToken(HttpServletRequest request) {
        try {
            Cookie[] cookies = request.getCookies();
            if(cookies.length> 0){
                for (Cookie cookie : cookies) {
                    System.out.println(cookie.getName());
                    if (cookie.getName().equals("token")) {
                        token = cookie.getValue();
                    }
                }
            }
        }catch(Exception ex){
            token = "";
        }

        return token;
    }

    private String decode(String token) {

        return JWT.require(Algorithm.HMAC512(SECRET.getBytes()))
                .build().verify(token.replace(TOKEN_PREFIX, ""))
                .getSubject();

    }


    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String home(Model model, HttpServletResponse response,HttpServletRequest request) {
        String tokenUser = getToken(request);
        try {
            if(!tokenUser.equals("")){
                String email = decode(tokenUser);
                Cookie cookie = new Cookie("email", email);
                response.addCookie(cookie);
                System.out.println(email);
            }
        }catch (Exception ex){

        }

        return "home";
    }

    @RequestMapping(value = {"/list-product"}, method = RequestMethod.GET)
    public String listProduct(Model model) {
        return "listProduct";
    }

    @RequestMapping(value = {"/product"}, method = RequestMethod.GET)
    public String detailProduct(Model model) {
        return "detailProduct";
    }

    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String login(Model model) {

        return "login";

    }

    @RequestMapping(value = {"/signIn"}, method = RequestMethod.GET)
    public String signIn(Model model) {
        return "signIn";
    }

    @RequestMapping(value = {"/user"}, method = RequestMethod.GET)
    public String user(Model model) {
        return "user";
    }

    @RequestMapping(value = {"/cart"}, method = RequestMethod.GET)
    public String cart(Model model) {
        return "cart";
    }

    @RequestMapping(value = {"/aboutme"}, method = RequestMethod.GET)
    public String aboutme(Model model) {
        return "aboutme";
    }

    @RequestMapping(value = {"/contact"}, method = RequestMethod.GET)
    public String contact(Model model) {
        return "contact";
    }

    @RequestMapping(value = {"/newdetail"}, method = RequestMethod.GET)
    public String newdetail(Model model) {
        return "newdetail";
    }

    @RequestMapping(value = {"/news"}, method = RequestMethod.GET)
    public String news(Model model) {
        return "news";
    }
}
