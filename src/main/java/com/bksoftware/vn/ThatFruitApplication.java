package com.bksoftware.vn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThatFruitApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThatFruitApplication.class, args);
    }

}
