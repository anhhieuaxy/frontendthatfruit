
$(document).ready(function () {
    getAboutmeProducts();
    // setSocial();
});

function setAboutmeProducts(products){
    var text = ``;
    products.map((data) => {
        text += `
    <div class="diet-items">
        <div class="di-img">
            <a href="product?id=${data.id}"><img src="${data.image}" alt="${data.name}"></a>
        </div>
        <div class="di-text">
            <p>${data.name}</p>
            <span>${data.productInfo}</span>
        </div>
    </div> `
    })
    $(".list-diet__wp").html(text);
}
function getAboutmeProducts() {
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: URL_API + `/v1/public/product/page?page=1` ,
        headers: {
            "adminbksoftwarevn": tokenHeader_value
        },
        timeout: 30000,
        success: function (result) {
            setAboutmeProducts(result);
        },
        error:  function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    })
}
// function setSocial() {
//     $.ajax({
//         type: 'GET',
//         dataType: "json",
//         url: URL_API + `v1/public/company/contact/all`,
//         headers: {
//             "adminbksoftwarevn": tokenHeader_value
//         },
//         timeout: 30000,
//         success: function (rs) {
//             console.log(rs);
//         },
//         error:  function (jqXHR, textStatus, errorThrown) {
//             console.log(errorThrown);
//         }
//     })
// }
