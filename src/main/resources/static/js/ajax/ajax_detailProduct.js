// // CONST
// const URL_API = "http://t:9900/api";
// const tokenHeader_value = "1df0e68d684175afa5ae2c3d1543fa0e";
$(document).ready(function () {
    getIdByUrl();
});



function getIdByUrl() {
    const urlDetailsProduct = window.location.href;
    var str = urlDetailsProduct.split("=");
    const id = str[str.length - 1];
    if ((id - 1) >= 0) {
        getDetailProduct(id);
        getInfoProduct(id);

    }

}

function getDetailProduct(id) {
    let tmp = "";
    $.ajax({
        type: "GET",
        url: URL_API+"/v1/public/product/find-by-id?id=" + id,
        headers: {
            "adminbksoftwarevn": tokenHeader_value
        },
        dataType: "json",
        success: function (data, indexof) {
            // language=HTML
            // console.log(data);
            tmp += `
           <div class="item-product detail-product add-id-cart" data-id=${data.id}>
                <div class="img-prduct dt-img-pr col-lg-5 col-md-6 col-sm-12 col-xs-12">
                    <img id="zoom_03"
                         src="${data.image}"
                         data-zoom-image="${data.image}" />
                    <div id="gallery_01">
                        <a data-image="${data.image}"
                           data-zoom-image="${data.image}">
                            <img
                                    src="${data.image}" />
                        </a>
                        <a data-image="${data.imageOne}"
                           data-zoom-image="${data.imageOne}">
                            <img
                                    src="${data.imageOne}" />
                        </a>
                        <a data-image="${data.imageTwo}"
                           data-zoom-image="${data.imageTwo}">
                            <img
                                    src="${data.imageTwo}" />
                        </a>
                        <a data-image="${data.imageThree}"
                           data-zoom-image="${data.imageThree}">
                            <img
                                    src="${data.imageThree}" />
                        </a>



                    </div>
                </div>
                <div class="name-product dt-name-pr col-lg-7 col-md-6 col-sm-12 col-xs-12">
                    <div class="fp__wapper">
                        <span class="fp__cap">${data.name}</span>
                        <i class="fp__price"><del>${data.originCostRetail}</del> ${data.saleCostWholesale}</i>
                        <div class="rating">
                            <span class="fa fa-star checked" aria-hidden="true"></span>
                            <span class="fa fa-star checked" aria-hidden="true"></span>
                            <span class="fa fa-star checked" aria-hidden="true"></span>
                            <span class="fa fa-star checked" aria-hidden="true"></span>
                            <span class="fa fa-star checked" aria-hidden="true"></span>
                        </div>
                        <hr class="divider">
                        <p>${data.productInfo}</p>
                  
                        <a class="fp__label click-add-cart"> Thêm vào giỏ</a>
                     
                    </div>
                    <div class="dt-danhmuc">
                        <div class="dt-dm-left">
                            <ul>
                                <li><span>Danh mục:</span></li>
                                <li><span>Tag:</span></li>
                                <li><span>Share:</span>
                                </li>
                            </ul>
                        </div>
                        <div class="dt-dm-right">
                            <ul>
                                <li>
                                    <a href="">Trái cây tươi</a>
                                </li>
                                <li>
                                    <a href="">Trái Cây</a>
                                    <a href="">${data.tags[0].name}</a>
                                    <a href="">${data.origin}</a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=${window.location.href}" title="chia sẻ trên Facebook" class="sicons"><i class="fab fa-facebook-f"></i></a>
                                    <a href="https://twitter.com/share?text=${data.name}&url=${window.location.href}" title="chia sẻ trên Twitter" class="sicons"><i class="fab fa-twitter"></i></a>
                                    <a href="https://plus.google.com/share?url=${window.location.href}" title="chia sẻ trên Google Plus" class="sicons"><i class="fab fa-google-plus-g" ></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
           `;
            $("#blockItemDetail").html(tmp);
            relatedProducts(data.smallCategory.id);
            clickAddCart("#featuredsproducts");
            $("#gallery_01 a").click(function(){
                var imgSrc=  $(this).find('img').attr("src");
                $("img#zoom_03").attr("src",imgSrc);
                return false;
            });
        },
        error: function (err) {
            console.log(err);

        }
    });
}

function getInfoProduct(id) {
    let infoPr = "";

    $.ajax({
        type: "GET",
        url:URL_API+ "/v1/public/product/find-by-id?id=" + id,
        headers: {
            "adminbksoftwarevn": tokenHeader_value
        },
        dataType: "json",
        success: function (data2, indexof) {
            // language=HTML

            infoPr += `
<div role="tabpanel tab-v2">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nv-tab" role="tablist">
                    <li role="presentation" class="active ">
                        <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Thông tin sản phẩm</a>
                    </li>

                </ul>

                <!-- Tab panes -->
                <div class="tab-content tab-ct-v2">
                    <div role="tabpanel" class="tab-pane active" id="home">
                        <div class="title-tab">
                            <span>Thông tin sản phẩm</span>
                        </div>
                        <div class="t-content">
                            <p>${data2.productInfo}</p>
                           
                        </div>
                    </div>

                </div>
            </div>
`;
            $("#info-prduct-detail").html(infoPr);

        },
        error: function (err) {
            console.log(err);

        }
    });
}

// GET SẢN PHẨM LIÊN QUAN
function relatedProducts(idSmall) {

    $.ajax({
        type: "GET",
        url: URL_API+"/v1/public/product/by-small-category/page?id-small=" + idSmall + "&page=1&size=4",
        dataType: "json",
        headers: {
            "adminbksoftwarevn": tokenHeader_value
        },
        success: function (data, indexof) {
            let tmp = "";

            const urlDetailsProduct2 = window.location.href;
            var str2 = urlDetailsProduct2.split("=");

            let dataFilter = data.filter(dt => dt.id != str2[str2.length-1]);

            for (let i = 0; i < 3; i++) {
                tmp += `<li class="col-lg-4 col-md-6 col-sm-6 col-xs-12 add-id-cart" data-id=${dataFilter[i].id}>
                  <div class="item-pr  detai-pr" >
                  <div class="item-pr-v2">
                <div class="img-prduct det" >
                    <div class="flip-box">
                        <div class="flip-box-inner">
                            <div class="flip-box-front">
                            <a href="product?id=${dataFilter[i].id}">
                             <img src="${dataFilter[i].image}" alt="">

</a>

                            </div>
                            <div class="flip-box-back">
                            <a href="product?id=${dataFilter[i].id}">
                               <img src="${dataFilter[i].imageOne}">
                                <i class="fas fa-eye"></i>

</a>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="name-product det">
                    <div class="fp__wapper">
                    <a href="product?id=${dataFilter[i].id}">
                     <span class="fp__cap">${dataFilter[i].name}</span>
</a>

                        <i class="fp__price"><del>${dataFilter[i].originCostRetail}</del> ${dataFilter[i].saleCostRetail}</i>
                        <div class="rating">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                        </div>
                        <hr class="divider">
                       
                       
                        <a class="fp__label click-add-cart"> Thêm vào giỏ</a>
                     
                    </div>
                </div>
</div>
            </div>
            </li>
                `;
                $("#rl-prduct").html(tmp);
                clickAddCart("#rl-prduct");
            }

        }
        , error: function (err) {
            console.log(err);


        }
    })


}
