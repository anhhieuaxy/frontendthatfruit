// // CONST
// const URL_API = "http://t:9900/api";
// const tokenHeader_value = "1df0e68d684175afa5ae2c3d1543fa0e";
var check = 0;
var size = 0;
var id;
var category;


$(document).ready(function () {
    checkUrl();

    // changeInterfaceProduct();

    $(".item-pr .item-pr-v2").addClass("it-prr");
    $(".it-prr .item-pr-v2 ").css({"display": "block", "width": "100%"});
    $(".item-pr ").addClass("col-md-6 col-lg-4 col-xs-6");
    $(".item-pr ").css({"max-width": "400px"});
    $(".name-product .fp__wapper p").hide();
    $(".head1").css({"display": "inline-block"});
    $(".head2").css({"display": "none"});
    $(".fp__wapper").css({"text-align": "center"});
    // click sắp xếp

   let href = new URL(window.location.href);
   let nameSearch = href.searchParams.get("name");
   if(nameSearch !== null) $("#searchHeader").val(nameSearch);
});

// lấy ra Product theo small or big
async function getProduct(id, category) {
    let arrCategory = new Array();
    let url_Ajax;
    if (category === "big-category") {
        url_Ajax = URL_API + "/v1/public/product/by-big-category/page?id-big=" + id;
        await $.ajax({
            type: "GET",
            url: url_Ajax,
            headers: {
                "adminbksoftwarevn": "1df0e68d684175afa5ae2c3d1543fa0e"
            },
            dataType: "json",
            success: function (result) {
                arrCategory = result;
            }
        });


    } else if (category === "small-category") {
        url_Ajax = URL_API + "/v1/public/product/by-small-category/page?id-small=" + id;
        await $.ajax({
            type: "GET",
            url: url_Ajax,
            headers: {
                "adminbksoftwarevn": "1df0e68d684175afa5ae2c3d1543fa0e"
            },
            dataType: "json",
            success: function (result) {

                arrCategory = result;
                console.log(arrCategory)
            }
        });
    }
    await $('#demo1').pagination({
        dataSource: arrCategory,
        pageSize: 9,
        callback: function (data2, pagination) {
            addInformationAmountProduct(data2);
            let html = addDataProduct(data2);

            $(".fa-prd").html(html);
            $(".item-pr .item-pr-v2").addClass("it-prr");
            $(".it-prr .item-pr-v2 ").css({"display": "block", "width": "100%"});
            $(".item-pr ").addClass("col-md-6 col-lg-4 col-xs-6");
            $(".item-pr ").css({"max-width": "400px"});
            $(".name-product .fp__wapper p").hide();
            $(".head1").css({"display": "inline-block"});
            $(".head2").css({"display": "none"});
            $(".fp__wapper").css({"text-align": "center"});
            clickAddCart("#featuredsproducts");
        },
        error: function (err) {
            console.log(err);
        }
    });
    arrCategory.sort(function (a, b) {
        return a.saleCostRetail - b.saleCostRetail;
    })
    $("#fildef").click(function () {
        getProduct(id, category);

    })
    $("#filprice").click(function () {
        priceSoft(arrCategory);
    })


}

function addDataProduct(data2) {
    let html = "";
    data2.map(function (data, indexof) {
        html += `<div class="item-pr  " >
                <div class="item-pr-v2 add-id-cart" data-id=${data.id}>
                <div class="img-prduct">
                    <div class="flip-box">
                        <div class="flip-box-inner">
                            <div class="flip-box-front">
                            <a href="product?id=${data.id}">
                             <img src="${data.image}" alt="">

</a>

                            </div>
                            <div class="flip-box-back">
                            <a href="product?id=${data.id}">
                               <img src="${data.imageOne}">
                                <i class="fas fa-eye"></i>

</a>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="name-product">
                    <div class="fp__wapper">
                    <a href="product?id=${data.id}">
                     <span class="fp__cap">${data.name}</span>
</a>

                        <i class="fp__price"><del> ${formatNumber(data.originCostRetail, '.', '.')} </del> ${formatNumber(data.saleCostRetail, '.', '.')} </i>
                        <div class="rating">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked" ></span>
                            <span class="fa fa-star checked" ></span>
                        </div>
                        <hr class="divider">
                        <p>${data.productInfo}</p>

                        <a  class="fp__label click-add-cart"> Thêm vào giỏ</a>

                    </div>
                </div>
            </div>
            </div>
            `
    });
    return html;

}

// function xắp xếp theo giá
function priceSoft(data) {

    $('#demo1').pagination({
        dataSource: data,
        pageSize: 9,
        callback: function (result, pagination) {


            console.log(result)
            // console.log(result);
            let ttList = "";
            if (result.length < 1) {
                ttList += ` <span> Không có sản phẩm nào để hiển thị</span>`;
            } else {
                ttList += ` <span> Hiển thị ${result.length} sản phẩm của kết quả</span>`;
            }
            $("#tt-list").html(ttList);
            let tmp = addDataProduct(result);
            $("#featuredsproducts .fa-prd").html(tmp);
            $(".item-pr .item-pr-v2").addClass("it-prr");
            $(".it-prr .item-pr-v2 ").css({"display": "block", "width": "100%"});
            $(".item-pr ").addClass("col-md-6 col-lg-4 col-xs-6");
            $(".item-pr ").css({"max-width": "400px"});
            $(".name-product .fp__wapper p").hide();
            $(".head1").css({"display": "inline-block"});
            $(".head2").css({"display": "none"});
            $(".fp__wapper").css({"text-align": "center"});
            // checkClick();
            clickAddCart("#featuredsproducts");
        },
        error: function (err) {
            console.log(err);
        }
    });
}
// check url
    function checkUrl() {
        let str = window.location.href.split("=");
        let str2 = window.location.href.split("/");
        let str3 = str2[str2.length - 1].toString().split("?");
        let str4 = str3[str3.length - 1].toString().split("=");
        let nameSearch = str4[str4.length - 1].split("=");
        let url = str4[0];
        id = str[str.length - 1];
        let checkUrl = str[0].split("?");
        category = checkUrl[checkUrl.length - 1];
        if (id !== null || id > 0) {
            console.log("aaaaaaa")
            getProduct(id, category);
        }
        if (url == "name") {
            serchNameProduct(nameSearch);
        }
    }

// ajax theo tên sản phẩm
    function serchNameProduct(url) {
        $('#demo1').pagination({
            dataSource: function (done) {
                $.ajax({
                    type: "GET",
                    url: URL_API + "/v1/public/product/name/all?name=" + url,
                    headers: {
                        "adminbksoftwarevn": tokenHeader_value
                    },
                    dataType: "json",
                    success: function (response) {
                        done(response);
                    }
                });
            },

            pageNumber: 1,
            pageSize: 6,
            callback: function (data2, pagination) {
                if (data2 != null) {
                    addInformationAmountProduct(data2);
                    let tmp = addDataProduct(data2);

                    $(".fa-prd").html(tmp);
                    $("#featuredsproducts .fa-prd").html(tmp);

                    $(".item-pr .item-pr-v2").addClass("it-prr");
                    $(".it-prr .item-pr-v2 ").css({"display": "block", "width": "100%"});
                    $(".item-pr ").addClass("col-md-6 col-lg-4 col-xs-6");
                    $(".item-pr ").css({"max-width": "400px"});
                    $(".name-product .fp__wapper p").hide();
                    $(".head1").css({"display": "inline-block"});
                    $(".head2").css({"display": "none"});
                    $(".fp__wapper").css({"text-align": "center"});
                    // checkClick();
                    clickAddCart("#featuredsproducts");
                } else {
                    addInformationAmountProduct(data2);
                    $(".fa-prd").html(`<span>Không có sản phẩm nào được tìm thấy</span>`);
                }
            }
        });
    }

/// thêm thông tin  số lượng sản phẩm cho mỗi trang
    function addInformationAmountProduct(data) {
        let ttList = "";
        if (data.length < 1) {
            ttList += ` <span> Không có sản phẩm nào để hiển thị</span>`;
        } else {
            ttList += ` <span> Hiển thị ${data.length} sản phẩm của kết quả</span>`;
        }
        $("#tt-list").html(ttList);
    }