
$(document).ready(function(){
    getCompanyMapCtu(1);
    getCompanyContactUs(1);
    checkUrlContactForm();
})
//  set map, get map

function setMapCtu(linkMap) {
    $('.map-google iframe').attr('src',linkMap);
}
function getCompanyMapCtu(id) {
    let contactCompany = [];
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: URL_API + `/v1/public/company/contact/company?company-id=${id}`,
        headers: {
            "adminbksoftwarevn": tokenHeader_value
        },
        timeout: 30000,
        success: function (result) {
            contactCompany = result;
            setMapCtu(contactCompany[0].map); //one company one contact
        },
        error:  function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    })
}


// set hotline
function setPhoneContactUs(hotline) {
    hotline = '0'+hotline;
    hotline = hotline.substr(0,4) + ' ' + hotline.substr(4,3) + ' ' + hotline.substr(7,3);
    $('.contact-info__wapper.hotlinem a').html(`<a>${hotline}</a>`);
    $('.contact-info__wapper.hotlinem a').attr("href",`tel:${hotline}`);
}
function setMailCompanyContactUs(mail) {
    $('.contact-info__wapper.company-mailctu a').html(`<a>${mail}</a>`);
    $('.contact-info__wapper.company-mailctu a').attr("href",`mailto:${mail}`);
}
function getCompanyContactUs(id) {
    let company = {};
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: URL_API + `/v1/public/company/find-by-id?id=${id}`,
        headers: {
            "adminbksoftwarevn": tokenHeader_value
        },
        timeout: 30000,
        success: function (result) {
            company = result;
            getContactCompany(company.id);
            setPhoneContactUs(company.phone);
            setAddressCompany(company.address);
            setMailCompanyContactUs(company.email);
        },
        error:  function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    })
    return company;
}
function postFeedBack(){
    let click = $("#contact-send").click(function () {
        let kq =false;
        var info;
        var email = $('#umail').val();

        if ($('#umail').val().match('^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$')&& $('#uphone').val().match("(09|01[2|6|8|9])+([0-9]{8})\\b")){

            var phone = $('#uphone').val().replace($('#uphone').val().charAt(0),'');
             info = {
                "checked"  : 'false',
                "title"    : 'Hỏi Đáp',
                "fullName" : $('#ufullname').val(),
                "phone"    : phone,
                "email"    : $('#umail').val(),
                "content"  : $('#ufb').val()
            };
             kq=true;
        }else{
            alterThatFruitWarning("Xin vui lòng kiểm tra lại thông tin")
        }
        if (kq){
            $.ajax({
                type: "POST",
                contentType: "application/json",
                headers: {
                    "adminbksoftwarevn": tokenHeader_value
                },
                url: URL_API + "/v1/public/contact-form",
                data: JSON.stringify(info),
                cache: false,
                timeout: 3000,
                success: function (result) {
                    alterThatFruitImage("Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất có thể");
                },
                error:  function (err) {
                    console.log(err);
                }
            })
        }
    });
}
function checkUrlContactForm() {
    postFeedBack();
}


