//RUN
$(function () {
    checkLogin();
    setSlider();
    setImgProductMenu();
    setSupperOfferProduct();
    setBestSallerProduct(0);
    setMenuBestSellerProduct();
    setSpecialProduct();
    setDealsMonthProduct();
    setNewHome();
    setImageFeedback();
})

//Set img Slide
function setSlider() {
    //img slider id 2 -> 8
    getListImagePage(2, 8).then(listImg => {
        if (listImg.length === 0) {
            return false;
        } else {
            listImg.map((data, index) => {
                // console.log(data);
                $(`#slider .carousel-inner .carousel-item:nth-child(${index + 1}) img`).attr("src", data.url);
            })
        }
    }).catch(err => {
        console.log(err);
    })
    $("#slider .download-appStore").click(function () {
        alterThatFruitImage("Chức năng đang phát triển. Vui Lòng thử lại sau!");
        return false;
    })
    $("#slider .download-googlePlay").click(function () {
        alterThatFruitImage("Chức năng đang phát triển. Vui Lòng thử lại sau!");
        return false;
    })
}

//Set img product-menu
function setImgProductMenu() {
    //img product menu 10 -> 15
    getListImagePage(10, 15).then(listImg => {
        console.log(listImg);
        if (listImg.length === 0) {
            return false;
        } else {
            listImg.map((data, index) => {
                $(`#productmenu .ps${index + 1}`).html(`<img src="${data.url}" alt="">`);
            })
        }
    }).catch(err => {
        console.log(err);
    })
}

//Set supperoffer product
function setSupperOfferProduct() {
    getSupperOfferProduct(4, 1).then(listSupperOffer => {
        let len = listSupperOffer.length;
        if (len === 0) {
            return false;
        } else {
            //add full 4 product supperoffer
            if (len < 4) {
                for (let i = len; i < 4; i++) {
                    listSupperOffer.push(listSupperOffer[0]);
                }
            }
            let textSupperOfferProduct = `<div class="col-12 text-center"><h3>Ưu đãi đặc biệt</h3></div>`;
            listSupperOffer.map((data, index) => {

                switch (index) {
                    case 0:
                    case 3:
                        textSupperOfferProduct += `<div class="col-12 col-md-4">
                            <div class="wapper so-wapper click-product" data-id=${data.id}>
                                <div class="sot-wapper">
                                    <p>${data.name}</p>
                                    <span>${formatNumber(data.saleCostRetail, '.', '.')} VNĐ/1Kg</span>
                                    <button class="btn" data-id=${data.id} type="button"> Xem ngay
                                        <i class="fas fa-long-arrow-alt-right"></i>
                                    </button>
                                </div>
                                <div class="supperoffer__selection of${index + 1}">
                                </div>
                            </div>
                        </div>`;
                        break;
                    case 1:
                        textSupperOfferProduct += `<div class="col-12 col-md-4 d-flex flex-column  justify-content-between">
                            <div class="wapper so-wapper1 click-product" data-id=${data.id}>
                                <div class="sot-wapper ef2">
                                    <p>Up to ${(((data.originCostRetail - data.saleCostRetail) / data.originCostRetail) * 100).toFixed(0)}% Off</p>
                                    <button class="btn" data-id=${data.id} type="button"> Xem ngay
                                        <i class="fas fa-long-arrow-alt-right"></i>
                                    </button>
                                </div>
                                <div class="supperoffer__selection of2"></div>
                            </div>`
                        break;
                    case 2:
                        textSupperOfferProduct += `<div class="wapper so-wapper1 click-product" data-id=${data.id}>
                            <div class="sot-wapper ef3">
                                <p>Up to ${(((data.originCostRetail - data.saleCostRetail) / data.originCostRetail) * 100).toFixed(0)}% Off</p>
                                <button class="btn" data-id=${data.id} type="button"> Xem ngay
                                    <i class="fas fa-long-arrow-alt-right"></i>
                                </button>
                            </div>
                            <div class="supperoffer__selection of3"></div>
                            </div></div>`
                        break;
                }
                $('#supperoffer .row').html(textSupperOfferProduct);//addHTML
                // set background
                listSupperOffer.map((data, index) => {
                    switch (index) {
                        case 0:
                        case 3:
                            $(`#supperoffer .of${index + 1}`).css("background-image", `url(${data.imageOne})`);
                            break;
                        case 1:
                        case 2:
                            $(`#supperoffer .of${index + 1}`).css("background-image", `url(${data.imageTwo})`);
                            break;
                    }
                })
                clickProduct();
            })
        }
    }).catch(err => {
        console.log(err);
    });
}

//Set menu bestsellers product
function setMenuBestSellerProduct() {
    getBigCategoryPage().then(list => {
        let textMenuBestSeller = '<li><a href="" data-id=0>Tất cả</a></li>';
        list.map(data => {
            textMenuBestSeller += `<li><a href="" data-id=${data.id}>${data.name}</a></li>`
        })
        $("#bestsellers ul.bs-list").html(textMenuBestSeller);//addHTML
        $("#bestsellers ul li:nth-child(1)").addClass("active");//set Active
        clickMenuBestSeller();
    }).catch(err => {
        console.log(err);
    })
}

//Set bestsellers product
function setBestSallerProduct(idSmallCategory) {
    getBestSellerProduct(idSmallCategory, 6, 1).then(listProduct => {
        //if 0 product change to all
        if (listProduct.length === 0) {
            setBestSallerProduct(0);
            $("#bestsellers ul li").removeClass("active");//remove actitve all
            $("#bestsellers ul li:nth-child(1)").addClass("active");//set Active
            alterThatFruitInfo("Danh mục hiện tại chưa có sản phẩm tương ứng.", 2000);
        } else {
            textBestSeller = '<div class="carousel-item active"><div class="row d-flex justify-content-center">';
            listProduct.map((data, index) => {
                // 3 product in 1 slider
                if (index === 3) {
                    textBestSeller += `</div></div><div class="carousel-item "><div class="row d-flex justify-content-center">`
                }
                textBestSeller += `<div class="p-1 p-data">
                                        <div class="card add-id-cart" data-id=${data.id}>
                                            <div class="card-img click-product-cart">
                                                <img src="${data.image}" alt='${data.name}'>
                                                <i class="fas fa-eye"></i>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title text-center">${data.name}</h5>
                                                <p class="card-text text-center">${formatNumber(data.saleCostRetail, '.', '.')} đ</p>
                                                <div class="card__wp">
                                                    
                                                    <a  class="btn btn-outline-success w-100 click-add-cart">Thêm vào giỏ</a>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>`
            })
            textBestSeller += '</div></div>';
            $("#bestsellers .col-12:nth-child(3) .carousel-inner").html(textBestSeller);
            setProductSliderResponsive("#bestsellers .carousel-inner");
            clickProductCart("#bestsellers");
            clickAddCart("#carousel3Column");
        }
    }).catch(err => {
        console.log(err);
    })
}

//Set Special Product

function setSpecialProduct() {
    getSpecialProduct(4, 1).then(list => {
        if (list.length === 0) {
            return false;
        } else {
            textSpecial = ''
            list.map((data, index) => {
                textSpecial += `<div class="carousel-item add-id-cart " data-id=${data.id}>
                                <div class="row">
                                    <div class="col-12 col-md-5">
                                        <div class="flip-box">
                                            <div class="flip-box-inner">
                                                <div class="flip-box-front">
                                                    <img src=${data.imageThree} alt='${data.name}'>
                                                </div>
                                                <div class="flip-box-back">
                                                    <img src="${data.image}" alt='${data.name}'> 
                                                    <i class="fas fa-eye"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <div class="fp__wapper ">
                                            <span class="fp__cap">${data.name}</span>
                                            <i class="fp__price"><del>${formatNumber(data.originCostRetail, '.', '.')} đ</del> ${formatNumber(data.saleCostRetail, '.', '.')} đ</i>
                                            <div class="rating">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                            </div>
                                            <hr class="divider">
                                            <p>${data.productInfo}</p>
                                            <a  class="fp__label click-add-cart"> Thêm vào giỏ</a>
                                          
                                         
                                        </div>
                                        <!--.row-->
                                    </div>
                                    <!--.item-->
                                </div>
                            </div>`
            })
            $(`#featuredsproducts .carousel-inner`).html(textSpecial);//addHTML
            $(`#featuredsproducts .carousel-inner .carousel-item:nth-child(1)`).addClass('active');
            clickAddCart("#featuredsproducts");
            clickFeaturedsProduct();
            // clickFeaturedsProductAddCart();
        }
    }).catch(err => {
        console.log(err);
    })
}

//Set dealsmonth product
function setDealsMonthProduct() {
    getBestSellersMonth(6, 1).then(list => {
        if (list.length === 0) {
            $('#dealsmonth').html('');//addHTML
            return false;
        } else {
            textMonth = `<div class="carousel-item active"><div class="row d-flex justify-content-center">`;
            list.map((data, index) => {
                // 3 product in 1 slider
                if (index === 3) {
                    textMonth += `</div></div><div class="carousel-item "><div class="row d-flex justify-content-center">`
                }
                textMonth += `<div class="p-1 p-data">
                                <div class="card add-id-cart" data-id=${data.id}>
                                    <a href="#" class="count">${convertDateToMilliseconds(data.enDateSale)}</a>
                                    <div class="card-img click-product-cart">
                                        <img src=${data.image} alt='${data.name}'>
                                        <i class="fas fa-eye"></i>
                                    </div>
                                    <div class="card-body">
                                        <h5 class="card-title text-center">${data.name}</h5>
                                        <p class="card-text text-center"><del>${formatNumber(data.originCostRetail, '.', '.')} đ</del> ${formatNumber(data.saleCostRetail, '.', '.')} đ <span class="sale">${(((data.originCostRetail - data.saleCostRetail) / data.originCostRetail) * 100).toFixed(0)}%</span></p>
                                        <div class="card__wp">
                                          
                                            <a  class="btn btn-outline-success w-100 click-add-cart">Thêm vào giỏ</a>
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>`
            })
            textMonth += '</div></div>';
            $('#dealsmonth .col-12:nth-child(2) .carousel-inner').html(textMonth);//addHTML
            setProductSliderResponsive("#dealsmonth .carousel-inner");
            //Run count time remain
            let arrTime = [];
            let length = $('#dealsmonth .carousel-inner .p-data').length;
            for (let i = 1; i <= length; i++) {
                arrTime.push($(`#dealsmonth .carousel-inner .p-data:nth-child(${i}) .count`).html()); //.text() error duplicate
            }
            setInterval(function () {
                runRemainTimeHome(arrTime);
            }, 1000);
        }
    }).catch(err => {
        console.log(err);
    })
}

//Set News
function setNewHome() {
    // return [[],[]];
    Promise.all([getNews(1, 1), getHotNews(1, 1)]).then(data => {

        if (data[0].length !== 0) {
            let date = data[0][0].time;
            textNews = `<div class="col-12"><h3 class="text-center">Tin tức</h3></div>`
            textNews += `<div class="col-12 col-md-6">
                    <img src="resources/img/salad1.png" alt='${data[0][0].title}' data-id=${data[0][0].id}>
                    <a href="../newdetail?id=${data[0][0].topic.id}" class="label1">${data[0][0].topic.name}</a>
                    <span class="newscap">${data[0][0].title}</span>
                    <time class="">by <strong>Thật Fruit</strong> <span>${convertTimeToView(data[0][0].time)}</span></time>
                    <p>${data[0][0].description}</p>
                </div>`;
            if (data[1].length !== 0) {
                let date1 = data[1][0].time;
                textNews += `<div class="col-12 col-md-6">
                    <a href="../newdetail?id=${data[0][0].topic.id}" class="label1">${data[0][0].topic.name}</a>
                    <span class="newscap">${data[1][0].title}</span>
                    <time class="">by <strong>Thật Fruit</strong> <span>${convertTimeToView(data[0][0].time)}</span></time>
                    <p>${data[0][0].description}</p>
                    <img src="resources/img/salad2.png" alt='${data[1][0].title}' data-id=${data[0][0].id}>
                </div>`;
                textNews += `<div class="col-12 d-flex justify-content-center">
                    <button class="btn" type="button"> Xem tất cả
                        <i class="fas fa-long-arrow-alt-right"></i>
                    </button>
                </div>`;
                $(`#news .row`).html(textNews);//addHTML
                $('#news img').click(function () {
                    let idNews = $(this).attr("data-id");
                    window.location.href = `/newdetail?id=${idNews}`;
                })
                $("#news .btn").click(function () {
                    window.location.href = "news";
                })
            }
        }
    }).catch(err => {
        console.log(err);
    })
}

//set image feedback home
function setImageFeedback() {
    //image feedback 16 -> 18
    getListImagePage(16, 18).then(list => {
        list.map((data, index) => {
            $(`#feedback .fb__wapper .img__wapper:nth-child(${index + 1}) img`).attr("src", data.url);
        })
    }).catch(err => {
        console.log(err);
    })
}

function runRemainTimeHome(arrTime) {
    let len = arrTime.length;
    for (let i = 1; i <= len; i++) {
        let textTime = arrTime[i - 1];
        arrTime[i - 1] = minusTimeRemain(textTime);
        $(`#dealsmonth .p-1:nth-child(${i}) .count`).text(minusTimeRemain(textTime));
    }
}

// Click menu Best Seller
function clickMenuBestSeller() {
    $('#bestsellers .bs-list li a').click(function () {
        setBestSallerProduct($(this).attr("data-id")); //get id smallCategory
        $("#bestsellers ul li").removeClass("active");//remove Active
        $(this).parent().addClass("active");//add Active
        return false;
    })
}

