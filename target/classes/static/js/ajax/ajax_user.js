const keyUser = JSON.parse(localStorage.getItem("infoUserLogin"));
var size;
$(document).ready(function () {
    getInforUser();
    getDataBuyForm();

})

function getInforUser() {
    let user = JSON.parse(localStorage.getItem("infoUser"));
    let tmp = "";
    if (user === null) {
        alterThatFruitWarning("Thông tin bạn chưa có vui lòng nhập thông tin để sử dụng chức năng này");
    } else {
        tmp += `
         <div class="usif-a">
                            <div class="usifa-img">
                                <img src="resources/img/user_male2-512.png" alt="">
                            </div>
                            <span>Thông tin </span>
                        </div>
                        <div class="usif-if">
                            <div class="us2if-wp">
                                <span>Tên</span>
                                <input id="name" type="text" value="${user.fullName}" required>
                            </div>
                            <div class="us2if-wp">
                                <span>Email</span>
                                <input id="email" type="text"value="${user.email}" pattern="^[a-z][a-z0-9_\\.]{5,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$" title="Vui lòng nhập đúng email" required>
                            </div>
                            <div class="us2if-wp">
                                <span>Số điện thoại</span>
                                <input id="phoneNumber" pattern="(09|01[2|6|8|9])+([0-9]{8})\\b" title="Vui lòng nhập đúng số điện thoại" type="text"value="" required>
                            </div>
                            <div class="us2if-wp">
                                <span>Địa chỉ</span>
                                <input id="address" type="text" value="" required>
                            </div>
                             <div class="us2if-info">
                               <button id="sbt-user" class="btn" style="background: var(--color-brown);color: white">Cập nhật</button>
                            </div>
                        </div>
                       
        `;
        $("#input-info-user-cart").html(tmp);
        updateData();
    }


}

function updateData() {
    let user = JSON.parse(localStorage.getItem("infoUser"));
    $("#sbt-user").click(function () {
        user.fullName = $("#name").val();
        user.phone = Number($("#phoneNumber").val().substr(1) - 0);
        user.email = $("#email").val();
        user.address = $("#address").val();

        $.ajax({
            type: "PUT",
            url: URL_API + "/v1/user/user/change-profile",
            headers: {
                "adminbksoftwarevn": tokenHeader_value,
                "Authorization": keyUser,
            },
            contentType: "application/json",
            timeout: 2000,
            data: JSON.stringify(user),
            success: function (response) {
                alterThatFruitSuccess("Cập nhật thông tin thành công. Vui lòng đăng nhập lại", 2000)
            },
            error: function (err) {
                console.log(err)
            }
        });

    })


}

// lấy ra số trang của buy form
function getSizeBuyForm() {
    let user = JSON.parse(localStorage.getItem("infoUser"));
    $.ajax({
        type: "GET",
        url: URL_API + "/v1/user/user/size?user-id=" + user.id,
        headers: {
            "adminbksoftwarevn": "1df0e68d684175afa5ae2c3d1543fa0e",
            "Authorization": keyUser,
        },
        session: 3000,
        async: false,
        dataType: "json",
        success: function (result) {
            size = result;

        },
        error: function (err) {
            console.log(err);
        }

    });
}

function updateData() {
    let user = JSON.parse(localStorage.getItem("infoUser"));
    $("#sbt-user").click(function () {
        user.fullName = $("#name").val();
        user.phone = Number($("#phoneNumber").val().substr(1) - 0);
        user.email = $("#email").val();
        user.address = $("#address").val();
        console.log(user)
        $.ajax({
            type: "PUT",
            url: URL_API + "/v1/user/user/change-profile",
            headers: {
                "adminbksoftwarevn": tokenHeader_value,
                "Authorization": keyUser,
            },
            contentType: "application/json",
            timeout: 2000,
            data: JSON.stringify(user),
            success: function (response) {
                alterThatFruitSuccess("Cập nhật thông tin thành công. Vui lòng đăng nhập lại", 2000)
            },
            error: function (err) {
                console.log(err)
            }
        });

    });

}

// lấy dữ liệu buy form từ database
function getDataBuyForm() {
    let user = JSON.parse(localStorage.getItem("infoUser"));
    getSizeBuyForm()
    let arr = new Array(size);
    let tmp = "";
    $('#demo2').pagination({
        dataSource: arr,
        pageNumber: 1,
        pageSize: 1,
        callback: function (data2, pagination) {
            $.ajax({
                type: "GET",
                url: URL_API + "/v1/user/user/buy-form?user-id=" + user.id + "&page=" + pagination.pageNumber,
                headers: {
                    "adminbksoftwarevn": tokenHeader_value,
                    "Authorization": keyUser,
                },
                dataType: "json",
                success: function (result) {


                    let html = "";
                    result.map(function (data, index) {
                        date1 = data.date;
                        date = date1.toString().replace(/,/g, "/");

                        html += `
                                <tr>
                                                        <td scope="row">${index + 1}</td>
                                                        <td>
                                                            <time><span>${date}</span></time>
                                                        </td>
                                                        <td  data-idForm=${data.id}>${data.id}</td>
                                                        <td> ${formatNumber(data.price, '.', '.')}</td>
                                                        <td class="tbdetail"><a onclick="clickDetailProduct(${data.id})">Chi tiết đơn hàng</a></td>
                                                    </tr>
                                `;
                    });
                    $("#tb-buyform").html(html);
                    $(".tbdetail").click(function () {
                        $("#orderdetail").css("display", "inline-flex");
                    });


                }
            });
            // console.log(data2)

        }
    });


}


// click xem chi tiết sản phẩm đã mua
function clickDetailProduct(id) {
    let user = JSON.parse(localStorage.getItem("infoUser"));
    let keyUser = JSON.parse(localStorage.getItem("infoUserLogin"));


    $.ajax({
        type: "GET",
        url: URL_API + "/v1/user/user/buy-form?user-id=" + user.id,
        headers: {
            "adminbksoftwarevn": tokenHeader_value,
            "Authorization": keyUser,
        },
        dataType: "json",
        success: function (data) {
            let result;
            data.map(function (item) {
                if (item.id == id) {
                    result = item;

                }
            })
            // var result2 = data.filter(pr => pr.id == id);
            // var result = result2;

            addDetailByForm(result);

        }
    });
}


function addDetailByForm(result) {
    if (result !== null || result.length != 0) {
        let tmp1 = "";
        tmp1 += `
                     <span>Mã đơn: <span>${result.id}</span></span>
                            <span class="ordt-cap">Chi tiết đơn hàng</span>
                            <span>Tổng tiền: <span>${result.price} đ</span></span>
                    `;

        let tmp = "";
        let quantity = result.quantity;
        result.products.map(function (product, index) {
            let price = 0;
            if (product.saleCostRetail != null || product.saleCostRetail != 0) {
                price = product.saleCostRetail;
            } else {
                price = product.originCostRetail;
            }
            tmp += `
                         <tr>
                                    <th scope="row"><img src="${product.image}" alt=""> <span>${product.name}</span>
                                    </th>
                                    <td> ${formatNumber(price, '.', '.')} đ</td>
                                    <td>${quantity[index]}</td>
                                    <td> ${formatNumber(price * quantity[index], '.', '.')} đ</td>
                                </tr>
                        `;
        });
        let html = `
                         <div class="ordt-top" id="codeForm">
                         ${tmp1}
                        </div>
                        <div class="ordt-table" >
                              <table class="table table-hover table-responsive-sm">
                                <thead>
                                <tr>
                                    <th scope="col"> Sản phẩm</th>
                                    <th scope="col">Giá</th>
                                    <th scope="col">Số lượng</th>
                                    <th scope="col">Tổng cộng</th>
                                </tr>
                                </thead>
                                <tbody class="tb-user"   style="max-height: 200px; overflow: auto" >
                                      ${tmp}
                                </tbody>
                            </table>
                        </div>
                        <div class="odc" id="odc">
                            <span>Đóng</span>
                        </div>
                  
                    `;
        $("#detailPr").html(html);
        $(".odc span").click(function () {
            $("#orderdetail").css("display", "none");
        });
        // $("#orderdetail").click(function () {
        //     $("#orderdetail").css("display", "none");
        // });
        $("#odc").click(function () {
            $('#detailPr').html("");
        });
    } else {
        $("#detailPr").html("Không có thông tin nào để hiển thị");
    }


    $("#detailPr").html(html);
    $(".odc span").click(function () {
        $("#orderdetail").css("display", "none");
    });
    $("#odc").click(function () {
        $('#detailPr').html("");
    });

}






