$(document).ready(function () {
    checkLogin();

    $("#submitSign").click(function () {
        checkInformation();
    });
});

// kieem tra email và so ten co dung dinh dạng hay khong
function checkInformation() {

    $(".form-group >input").map(function () {
        if ($(this).val() == "") {
            $(this).siblings(".error-sign").css({"display": "inline"});
        } else {
            $(this).siblings(".error-sign").css({"display": "none"});
        }
    })
    let check = false;
    let arr = $(".form-group >input").map(function () {
        return $(this).val();
    })
    for (let i = 0; i <arr.length; i++) {
        if (arr[i] == "") {
            break;
        } else {
            check = true;
        }
    }
    if (check == true){
        if ($("#name").val().match("(.{3,11})") && $("#email").val().match("(^[a-z][a-z0-9_\\.]{5,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$)")) {
            checkPassword();
        } else {
            alterThatFruitWarning("Vui lòng nhập đúng định dạng email", 1500);
        }

    }

}

// kiem tra password 1 với 2 xem có trung nhau hay khong
function checkPassword() {
    if ( $("#password-v2").val() !=="" && $("#password").val()!=="") {
        if ($("#password-v2").val() == $("#password").val()) {
            postUser();
        }else {
            alterThatFruitWarning("Mật khẩu không khớp",1500)
        }
    }

}

// post thong tin user len database
function postUser() {
    let user = {
        "email": $("#email").val(),
        "password": $("#password").val(),
        "fullName": $("#name").val()
    }
    $.ajax({
        type: "POST",
        url: URL_API + "/v1/public/user/register",
        data: JSON.stringify(user),
        headers: {
            "adminbksoftwarevn": tokenHeader_value
        },
        contentType: "application/json",
        timeout: 2000,
        success: function (response) {
            alterThatFruitImage('Đăng kí thành công', 1000)
            localStorage.setItem("infoUser", JSON.stringify(response));
            document.cookie = "dataInfo=" + response.id;
            setTimeout(function () {
                location.href = "home"
            }, 1500);
        },
        error: function (err) {
            alterThatFruitWarning("Email đã được sử dụng, xin vui lòng thử một tài khoản khác")
            console.log(err);
        }
    });
}
