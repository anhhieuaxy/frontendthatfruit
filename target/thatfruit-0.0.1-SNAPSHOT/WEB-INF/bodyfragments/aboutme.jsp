<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="resources/js/ajax/ajax_aboutme.js"></script>

<main>
    <section id="banner">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="banner__wp">
                        <div class="banner-text">
                            <h4>Giới thiệu</h4>
                            <a href="home">Trang chủ</a>
                            <span>// Giới thiệu</span>
                        </div>
                        <div class="banner-bg">
                            <img src="resources/img/banner.png" alt="">
                        </div>
                        <span class="bor bor1"></span>
                        <span class="bor bor2"></span>
                        <span class="bor bor4"></span>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section id="aboutme-diet">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h3>Trái cây và sức khỏe</h3>
                </div>
                <div class="col-12 col-lg-4 col-md-12 col-sm-12">
                    <div class="list-diet__wp">

                    </div>
                </div>
                <div class="col-md-12 col-12 col-lg-8 col-sm-12">
                    <div class="di-wp">
                        <div class="row">
                            <div class="col-md-12 col-lg-6 col-12 col-sm-12">
                                <div class="dw-img">
                                    <img src="resources/img/health.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 col-12 col-sm-12">
                                <div class="dw-img">
                                    <img src="resources/img/shape4.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="aboutme-fb">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3>MỌI NGƯỜI NÓI VỀ CHÚNG TÔI</h3>
                </div>
                <div class="col-12">
                    <div id="carousel3Column12" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel3Column12" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel3Column12" data-slide-to="1"></li>
                            <li data-target="#carousel3Column12" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row">

                                    <div class="col-12">
                                        <div class="aboutme__fb-wp">
                                            <div class="abmefbwp-img">
                                                <img src="resources/img/user_male2-512.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="abmefbwp-text">
                                            <p>"Trái cây của các bạn rất tuyệt chúng tôi rất tin tưởng vào chất lượng của mỗi sản phẩm "</p>
                                            <span class="ver"></span>
                                            <span class=" d-block fb-name">Nguyễn Trọng Hoàng</span>
                                            <span class=" d-block">Our happy customer</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item ">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="aboutme__fb-wp">
                                            <div class="abmefbwp-img">
                                                <img src="resources/img/user_male2-512.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="abmefbwp-text">
                                            <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                eiusmod tempor incididunt ut
                                                labore et dolore magna aliqua. Quis ipsum suspendisse ultrices
                                                gravida. Risus commodo
                                                viverra maecenas accumsan lacus vel facilisis.”</p>
                                            <span class="ver"></span>
                                            <span class=" d-block fb-name">Adam Gilchrit</span>
                                            <span class=" d-block">Our happy customer</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item ">
                                <div class="row">

                                    <div class="col-12">
                                        <div class="aboutme__fb-wp">
                                            <div class="abmefbwp-img">
                                                <img src="resources/img/user_male2-512.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="abmefbwp-text">
                                            <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                eiusmod tempor incididunt ut
                                                labore et dolore magna aliqua. Quis ipsum suspendisse ultrices
                                                gravida. Risus commodo
                                                viverra maecenas accumsan lacus vel facilisis.”</p>
                                            <span class="ver"></span>
                                            <span class=" d-block fb-name">Adam Gilchrit</span>
                                            <span class=" d-block">Our happy customer</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="aboutme-dedfar">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3>NÔNG DÂN TẬN TÂM</h3>
                </div>

                <div class="col-12">
                    <div id="aboutmeslider" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="abmdf__wp">
                                            <div class="abmdfwp-img">
                                                <img src="resources/img/farm1.png" alt="">
                                            </div>
                                            <div class="abmdfwp-text">
                                                <span class="abmdt-name">Fanbong Farm</span>
                                                <span class="abmdt-decs"> Chartered Financial Advisor</span>
                                                <span class="abmdt-hr"></span>
                                                <p>Born in Belmont, CA, Kathleen attended UC Riverside where she
                                                    earned her B.S. </p>
                                                <div class="abmdt-icon">
                                                    <a href="#"><img src="resources/img/fb.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/tw.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/yt.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/g+.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/ins.png" alt=""></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="abmdf__wp">
                                            <div class="abmdfwp-img">
                                                <img src="resources/img/farm2.png" alt="">
                                            </div>
                                            <div class="abmdfwp-text">
                                                <span class="abmdt-name">Fanbong Farm</span>
                                                <span class="abmdt-decs"> Chartered Financial Advisor</span>
                                                <span class="abmdt-hr"></span>
                                                <p>Born in Belmont, CA, Kathleen attended UC Riverside where she
                                                    earned her B.S. </p>
                                                <div class="abmdt-icon">
                                                    <a href="#"><img src="resources/img/fb.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/tw.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/yt.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/g+.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/ins.png" alt=""></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item ">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="abmdf__wp">
                                            <div class="abmdfwp-img">
                                                <img src="resources/img/farm1.png" alt="">
                                            </div>
                                            <div class="abmdfwp-text">
                                                <span class="abmdt-name">Fanbong Farm</span>
                                                <span class="abmdt-decs"> Chartered Financial Advisor</span>
                                                <span class="abmdt-hr"></span>
                                                <p>Born in Belmont, CA, Kathleen attended UC Riverside where she
                                                    earned her B.S. </p>
                                                <div class="abmdt-icon">
                                                    <a href="#"><img src="resources/img/fb.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/tw.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/yt.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/g+.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/ins.png" alt=""></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="abmdf__wp">
                                            <div class="abmdfwp-img">
                                                <img src="resources/img/farm2.png" alt="">
                                            </div>
                                            <div class="abmdfwp-text">
                                                <span class="abmdt-name">Fanbong Farm</span>
                                                <span class="abmdt-decs"> Chartered Financial Advisor</span>
                                                <span class="abmdt-hr"></span>
                                                <p>Born in Belmont, CA, Kathleen attended UC Riverside where she
                                                    earned her B.S. </p>
                                                <div class="abmdt-icon">
                                                    <a href="#"><img src="resources/img/fb.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/tw.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/yt.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/g+.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/ins.png" alt=""></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item ">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="abmdf__wp">
                                            <div class="abmdfwp-img">
                                                <img src="resources/img/farm1.png" alt="">
                                            </div>
                                            <div class="abmdfwp-text">
                                                <span class="abmdt-name">Fanbong Farm</span>
                                                <span class="abmdt-decs"> Chartered Financial Advisor</span>
                                                <span class="abmdt-hr"></span>
                                                <p>Born in Belmont, CA, Kathleen attended UC Riverside where she
                                                    earned her B.S. </p>
                                                <div class="abmdt-icon">
                                                    <a href="#"><img src="resources/img/fb.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/tw.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/yt.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/g+.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/ins.png" alt=""></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="abmdf__wp">
                                            <div class="abmdfwp-img">
                                                <img src="resources/img/farm2.png" alt="">
                                            </div>
                                            <div class="abmdfwp-text">
                                                <span class="abmdt-name">Fanbong Farm</span>
                                                <span class="abmdt-decs"> Chartered Financial Advisor</span>
                                                <span class="abmdt-hr"></span>
                                                <p>Born in Belmont, CA, Kathleen attended UC Riverside where she
                                                    earned her B.S. </p>
                                                <div class="abmdt-icon">
                                                    <a href="#"><img src="resources/img/fb.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/tw.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/yt.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/g+.png" alt=""></a>
                                                    <a href="#"><img src="resources/img/ins.png" alt=""></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#aboutmeslider" data-slide="prev">
                                <span class="carousel-control-prev-icon"><i
                                        class="fas fa-long-arrow-alt-left"></i></span>
                        </a>
                        <a class="carousel-control-next" href="#aboutmeslider" data-slide="next">
                                <span class="carousel-control-next-icon"><i
                                        class="fas fa-long-arrow-alt-right"></i></span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- mail area -->
    <section id="mail-area">
        <div class="container">
            <div class="row d-flex align-items-center justify-content-around">
                <div class="col-md-3 col-sm-12">
                    <p class="text-center m-0">Đăng kí để nhận lời khuyên, tin tức và khuyến mãi từ Thật Fruit</p>
                </div>
                <div class="col-md-5 col-sm-12">
                    <div class="input-group">
                        <input type="text" class="  search-query form-control" placeholder="Emai của bạn" id="valsmail" />
                        <span class="input-group-btn">
              <button class="btn" type="button" id="btsmail" onclick="setFormFeedBack()">
                Đăng kí
              </button>
            </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end mail area -->
</main>