<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="resources/js/ajax/ajax_login.js"></script>
<main>
    <section id="banner">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="banner__wp">
                        <div class="banner-text">
                            <h4>Giới thiệu</h4>
                            <a href="home">Trang chủ</a>
                            <span>// Đăng Nhập</span>
                        </div>
                        <div class="banner-bg">
                            <img src="resources/img/banner.png" alt="">
                        </div>
                        <span class="bor bor1"></span>
                        <span class="bor bor2"></span>
                        <span class="bor bor4"></span>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section class="block-login container">
        <div class="row">
            <div class="body-content col-12">
                <div class="title-log">
                    <span>Đăng nhập</span>
                </div>
                <div class="body-form col-lg-6">
                    <div class=" form-login ">
                        <legend>Tài khoản của tôi</legend>

                        <div class="form-group">
                            <label for=""><span>Email:</span> <span class="error-log">Vui lòng điền vào đây</span></label>
                            <input id="username" required type="text" class="form-control" id=""
                                   pattern="^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$"
                                   title="Nhập email hợp lệ" value="">
                        </div>
                        <div class="form-group">
                            <label for=""><span>Mật khẩu:</span> <span class="error-log">Vui lòng điền vào đây</span></label>
                            <input id="password" required type="password" class="form-control" id="" value="">
                        </div>


                        <div class="button-log">
                            <button id="submit"
                                    class="btn ">Đăng nhập</button>

                        </div>

                    </div>
                    <div class="bottom-log ">
                        <span>Bạn chưa có tài khoản? <a href="signIn">Đăng ký</a></span>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section id="mail-area">
        <div class="container">
            <div class="row d-flex align-items-center justify-content-around">
                <div class="col-md-3 col-sm-12">
                    <p class="text-center m-0">Đăng kí để nhận lời khuyên, tin tức và khuyến mãi từ Thật Fruit</p>
                </div>
                <div class="col-md-5 col-sm-12">
                    <div class="input-group">
                        <input type="text" class="  search-query form-control" placeholder="Emai của bạn" id="valsmail" />
                        <span class="input-group-btn">
              <button class="btn" type="button" id="btsmail" onclick="setFormFeedBack()">
                Đăng kí
              </button>
            </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>