<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="resources/js/ajax/ajax_cart.js"></script>

<main>

    <section id="banner">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="banner__wp">
                        <div class="banner-text">
                            <h4>Giỏ Hàng</h4>
                            <a href="home">Trang chủ</a>
                            <span>// Giỏ Hàng</span>
                        </div>
                        <div class="banner-bg">
                            <img src="resources/img/banner6.jpg" alt="">
                        </div>
                        <span class="bor bor1"></span>
                        <span class="bor bor2"></span>
                        <span class="bor bor3"></span>
                        <span class="bor bor4"></span>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section id="cart">
        <div class="container">
            <div class="row">
                <div class="col-12 row">
                    <div id="userinfo" class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div id="input-info-user-cart" class="userinfo-wp">
                            <div class="usif-a">

                                <span>Thông tin khách hàng:</span>
                            </div>
                            <div class="usif-if">
                                <div class="us2if-wp">
                                    <span>Tên</span>
                                    <input id="name" type="text" value="" required>
                                </div>
                                <div class="us2if-wp">
                                    <span>Email</span>
                                    <input id="email" type="text" value=""
                                           pattern="^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$"
                                           title="Vui lòng nhập đúng email" required>
                                </div>
                                <div class="us2if-wp">
                                    <span>Số điện thoại</span>
                                    <input id="phoneNumber" pattern="(09|01[2|6|8|9])+([0-9]{8})\b"
                                           title="Vui lòng nhập đúng số điện thoại" type="text" value="" required>
                                </div>
                                <div class="us2if-wp">
                                    <span>Địa chỉ</span>
                                    <input id="address" type="text" value="" required>
                                </div>

                                <div class="us2if-wp">
                                    <span>Yêu cầu đối với nhà cung cấp</span>
                                    <input id="request" type="text">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                        <div class="carttb-wp">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                    <th scope="col">Sản phẩm</th>
                                    <th scope="col">Giá</th>
                                    <th scope="col">Số lượng</th>
                                    <th scope="col">Tổng cộng</th>
                                </tr>
                                </thead>
                                <tbody class="tbody-card" id="tb-cart">


                                </tbody>
                            </table>
                        </div>

                        <div class="col-12 d-flex justify-content-sm-end justify-content-center">
                            <div class="cor-wp">

                                <table class="table table-hover">
                                    <tbody id="table-money">

                                    </tbody>
                                </table>
                                <button id="submit-cart" class="btn">Tiến hành đặt hàng</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- mail area -->
    <section id="mail-area">
        <div class="container">
            <div class="row d-flex align-items-center justify-content-around">
                <div class="col-md-3 col-sm-12">
                    <p class="text-center m-0">Đăng kí để nhận lời khuyên, tin tức và khuyến mãi từ Thật Fruit</p>
                </div>
                <div class="col-md-5 col-sm-12">
                    <div class="input-group">
                        <input type="text" class="  search-query form-control" placeholder="Emai của bạn"
                               id="valsmail"/>
                        <span class="input-group-btn">
              <button class="btn" type="button" id="btsmail" onclick="setFormFeedBack()">
                Đăng kí
              </button>
            </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end mail area -->
</main>