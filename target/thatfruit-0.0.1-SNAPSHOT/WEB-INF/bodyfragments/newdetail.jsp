<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="resources/js/ajax/ajax_newdetail.js"></script>

<main>
    <section id="banner">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="banner__wp">
                        <div class="banner-text">
                            <h4>Blog</h4>
                            <a href="home">Trang chủ</a>
                            <span>// Blog</span>
                        </div>
                        <div class="banner-bg">
                            <img src="resources/img/banner2.jpg" alt="">
                        </div>
                        <span class="bor bor1"></span>
                        <span class="bor bor2"></span>
                        <span class="bor bor3"></span>
                        <span class="bor bor4"></span>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section id="newsdetail">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3">
                    <div class="ndsearch">
                        <div class="input-group ">
                            <input class="form-control" type="text" placeholder="Tìm kiếm" aria-label="Search" id="keysearchnews">
                            <div class="input-group-append " id="newdetailsearch">
                                    <span class="input-group-text" id="ndsearchbt"><i class="fas fa-search text-grey"
                                                                                      aria-hidden="true"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="ndplist">
                        <span class="ndplist-cap">Danh mục sản phẩm</span>
                        <ul class="ndplist-list">
                        </ul>
                    </div>
                    <div class="ndrelatedp">
                        <span class="ndrelatedp-cap">Sản phẩm liên quan</span>
                        <%--                        <div class="ndrelatedp-wp">--%>
                        <%--                            <div class="ndrwp-img">--%>
                        <%--                                <img src="resources/img/strawberry6.png" alt="">--%>
                        <%--                                <div class="ndrwpi-mb">--%>
                        <%--                                    <i class="fas fa-eye"></i>--%>
                        <%--                                </div>--%>
                        <%--                            </div>--%>
                        <%--                            <div class="ndrwp-text">--%>
                        <%--                                <a class="ndrwpt-cap" href="#">Dâu tây Đà Lạt</a>--%>
                        <%--                                <i class="ndr-price">125.000 đ</i>--%>
                        <%--                                <div class="rating">--%>
                        <%--                                    <span class="fa fa-star checked"></span>--%>
                        <%--                                    <span class="fa fa-star checked"></span>--%>
                        <%--                                    <span class="fa fa-star checked"></span>--%>
                        <%--                                    <span class="fa fa-star"></span>--%>
                        <%--                                    <span class="fa fa-star"></span>--%>
                        <%--                                </div>--%>
                        <%--                            </div>--%>
                        <%--                        </div>--%>
                        <%--                        <div class="ndrelatedp-wp">--%>
                        <%--                            <div class="ndrwp-img">--%>
                        <%--                                <img src="resources/img/strawberry6.png" alt="">--%>
                        <%--                                <div class="ndrwpi-mb">--%>
                        <%--                                    <i class="fas fa-eye"></i>--%>
                        <%--                                </div>--%>
                        <%--                            </div>--%>
                        <%--                            <div class="ndrwp-text">--%>
                        <%--                                <a class="ndrwpt-cap" href="#">Dâu tây Đà Lạt</a>--%>
                        <%--                                <i class="ndr-price">125.000 đ</i>--%>
                        <%--                                <div class="rating">--%>
                        <%--                                    <span class="fa fa-star checked"></span>--%>
                        <%--                                    <span class="fa fa-star checked"></span>--%>
                        <%--                                    <span class="fa fa-star checked"></span>--%>
                        <%--                                    <span class="fa fa-star"></span>--%>
                        <%--                                    <span class="fa fa-star"></span>--%>
                        <%--                                </div>--%>
                        <%--                            </div>--%>
                        <%--                        </div>--%>
                        <%--                        <div class="ndrelatedp-wp">--%>
                        <%--                            <div class="ndrwp-img">--%>
                        <%--                                <img src="resources/img/strawberry6.png" alt="">--%>
                        <%--                                <div class="ndrwpi-mb">--%>
                        <%--                                    <i class="fas fa-eye"></i>--%>
                        <%--                                </div>--%>
                        <%--                            </div>--%>
                        <%--                            <div class="ndrwp-text">--%>
                        <%--                                <a class="ndrwpt-cap" href="#">Dâu tây Đà Lạt</a>--%>
                        <%--                                <i class="ndr-price">125.000 đ</i>--%>
                        <%--                                <div class="rating">--%>
                        <%--                                    <span class="fa fa-star checked"></span>--%>
                        <%--                                    <span class="fa fa-star checked"></span>--%>
                        <%--                                    <span class="fa fa-star checked"></span>--%>
                        <%--                                    <span class="fa fa-star"></span>--%>
                        <%--                                    <span class="fa fa-star"></span>--%>
                        <%--                                </div>--%>
                        <%--                            </div>--%>
                        <%--                        </div>--%>
                        <%--                        <div class="ndrelatedp-wp ndrlnb">--%>
                        <%--                            <div class="ndrwp-img">--%>
                        <%--                                <img src="resources/img/strawberry6.png" alt="">--%>
                        <%--                                <div class="ndrwpi-mb">--%>
                        <%--                                    <i class="fas fa-eye"></i>--%>
                        <%--                                </div>--%>
                        <%--                            </div>--%>
                        <%--                            <div class="ndrwp-text">--%>
                        <%--                                <a class="ndrwpt-cap" href="#">Dâu tây Đà Lạt</a>--%>
                        <%--                                <i class="ndr-price">125.000 đ</i>--%>
                        <%--                                <div class="rating">--%>
                        <%--                                    <span class="fa fa-star checked"></span>--%>
                        <%--                                    <span class="fa fa-star checked"></span>--%>
                        <%--                                    <span class="fa fa-star checked"></span>--%>
                        <%--                                    <span class="fa fa-star"></span>--%>
                        <%--                                    <span class="fa fa-star"></span>--%>
                        <%--                                </div>--%>
                        <%--                            </div>--%>
                        <%--                        </div>--%>
                    </div>
                    <div class="ndtags">
                        <span class="ndtags-cap">Search by Tags</span>
                        <%--                        <a href="#">Fruit</a>--%>
                        <%--                        <a href="#">Food</a>--%>
                        <%--                        <a href="#">Green</a>--%>
                        <%--                        <a href="#">Healthy</a>--%>
                        <%--                        <a href="#">Tips</a>--%>
                        <%--                        <a href="#">Natural</a>--%>
                        <%--                        <a href="#">Tomatos</a>--%>
                    </div>
                </div>
                <div class="col-12 col-md-9">
                    <div class="ndarticle">
                        <%--                        <span class="d-block ar-cap ">This is pretty cool</span>--%>
                        <%--                        <div class="artd-wp">--%>
                        <%--                            <span> Danh mục: <a href="#">Nước Ép </a></span> //--%>
                        <%--                            <span> Ngày: <time>April 27,2019 </time></span>--%>
                        <%--                        </div>--%>
                        <%--                        <div class="arccontent">--%>
                        <%--                            <div class="ndarcbimg-wp">--%>
                        <%--                                <img src="resources/img/newspost-img.png" alt="">--%>
                        <%--                                <div class="ndarcbimg-b"></div>--%>
                        <%--                            </div>--%>
                        <%--                            <p>We applaud their efforts, but you don’t have to commit to dumpster-diving to live a--%>
                        <%--                                forag-friendly lifestyle. One of the best ways to incorporate some freeganism into your life--%>
                        <%--                                is to take advantage of the wild fruits in your area. Odds are much of it is going to waste--%>
                        <%--                                anyways. Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam. Phasellus--%>
                        <%--                                pharetra nulla ac diam. Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam.--%>
                        <%--                                Etiam pellentesque aliquet tellus.</p>--%>
                        <%--                            <figure class="quote">--%>
                        <%--                                <blockquote class="curly-quotes">--%>
                        <%--                                    <i class="fas fa-quote-left"></i>--%>
                        <%--                                    The biggest problem was convincing my father that organic food was worth eating. All he--%>
                        <%--                                    could think of was the nut loaf with yeast gravy that my mother made in the Seventies.--%>
                        <%--                                </blockquote>--%>
                        <%--                                <figcaption class="quote-by">Fanbong</figcaption>--%>
                        <%--                            </figure>--%>
                        <%--                            <div class="arit-wp">--%>
                        <%--                                <div class="row">--%>
                        <%--                                    <div class="col-12 col-md-5">--%>
                        <%--                                        <div class="aritimage">--%>
                        <%--                                            <img src="resources/img/arimage1.png" alt="">--%>
                        <%--                                            <div class="aritimage-b"></div>--%>
                        <%--                                        </div>--%>
                        <%--                                    </div>--%>
                        <%--                                    <div class="col-12 col-md-7">--%>
                        <%--                                        <div class="aritwp-t">--%>
                        <%--                                            <span class="aritwp-t__cap">Disturb the soil as possible</span>--%>
                        <%--                                            <p>Soil health starts with one basic principle: Don’t disturb the dirt. Sure, soil--%>
                        <%--                                                supports roots and helps hold up plants, but it also serves as a habitat for--%>
                        <%--                                                beneficial microorganisms. “Underneath our feet is this incredible world teeming--%>
                        <%--                                                with billions of microor ganisms that have been working in the soil for millions of--%>
                        <%--                                                years,” Ohlson says. It may sound counter-intuitive—maybe even chaotic, in terms of--%>
                        <%--                                                landscaping. —but weeds don’t need to be treated as an enemy.--%>
                        <%--                                            </p>--%>
                        <%--                                        </div>--%>
                        <%--                                    </div>--%>
                        <%--                                </div>--%>

                        <%--                            </div>--%>
                        <%--                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut--%>
                        <%--                                labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo--%>
                        <%--                                viverra maecenas accumsan lacus vel facilisis. </p>--%>
                        <%--                        </div>--%>
                        <%--                        <div class="arcshare-wp">--%>
                        <%--                            <div class="arc-tags">--%>
                        <%--                                <span> Tag:</span> <a href="#">Green</a> , <a href="#">Natural</a>--%>
                        <%--                            </div>--%>
                        <%--                            <div class="arc-share">--%>
                        <%--                                <span> Share:</span> <a href="#"><i class="fab fa-facebook-f"></i></a>--%>
                        <%--                                <a href="#"><i class="fab fa-twitter"></i></a>--%>
                        <%--                                <a href="#"><i class="fab fa-google-plus-g"></i></a>--%>
                        <%--                            </div>--%>
                        <%--                        </div>--%>
                        <%--                        <div class="arc-np">--%>
                        <%--                            <a href="#"> <i class="fas fa-long-arrow-alt-left"></i>Trước</a>--%>
                        <%--                            <a href="#">Sau <i class="fas fa-long-arrow-alt-right"></i></a>--%>
                        <%--                        </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- mail area -->
    <section id="mail-area">
        <div class="container">
            <div class="row d-flex align-items-center justify-content-around">
                <div class="col-md-3 col-sm-12">
                    <p class="text-center m-0">Đăng kí để nhận lời khuyên, tin tức và khuyến mãi từ Thật Fruit</p>
                </div>
                <div class="col-md-5 col-sm-12">
                    <div class="input-group">
                        <input type="text" class="  search-query form-control" placeholder="Emai của bạn" id="valsmail" />
                        <span class="input-group-btn">
              <button class="btn" type="button" id="btsmail" onclick="setFormFeedBack()">
                Đăng kí
              </button>
            </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end mail area -->
</main>

