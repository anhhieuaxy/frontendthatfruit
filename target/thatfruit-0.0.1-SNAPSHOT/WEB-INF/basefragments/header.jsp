<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!-- header -->
<header id="header">
    <div class="container d-flex justify-content-md-end justify-content-center align-items-center header-list">
        <div class="account">
            <div class="dropdown">
          <span class="dropdown-toggle m-0 d-inline" id="dropdownMenuButton" >
            <i class="fas fa-user"></i> Tài khoản
          </span>
                <div class="dropdown-menu" >
                    <a class="dropdown-item" href="login">Đăng nhập</a>
                    <a class="dropdown-item" href="signIn">Đăng ký</a>
                    <a class="dropdown-item" id="out" >Đăng xuất</a>
                </div>
            </div>
        </div>
<%--        <a href="#" class="border-right"><i class="fas fa-heart "></i> </a>--%>
        <a href="#" class="address-company"><i class="fas fa-map-marker-alt"></i> Bản đồ</a>
    </div>
</header>
<!-- end header -->
<!-- search area -->
<section id="search-area">
    <div class="container ">

        <div class=" content-search row">
            <div class="input-group col-lg-6 col-md-12">
                <div class="ipg-wp">
                    <input type="text" class="  search-query form-control" id="searchHeader" placeholder="Tìm kiếm sản phẩm" />
                    <span class="input-group-btn">
                        <button class="btn" type="button">
                            <i id="clickSearch" class="fas fa-search"></i>
                        </button>
                    </span>
                </div>
            </div>
            <div class="header-right col-lg-6 col-md-12">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08"
                        aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
                </button>
                <div class=" logo">
                    <a href="home" class="logo-company"><img src="resources/img/logo11.png" alt="ThatFruit"></a>
                </div>
                <div id="car1" class="cart1">
                    <button class="card-item1 btn card1" type="button">
                        <i class="fas fa-cart-arrow-down"></i><span> 0sp-</span> <span>0 VNĐ</span>
                    </button>
                </div>
            </div>


        </div>
    </div>
</section>
<!-- end search area -->
<!-- nav -->
<nav class="navbar navbar-expand-lg navbar-dark">
    <div class="container">
        <a href="home"><img src="resources/img/logo11.png" alt="" id="logoNav"></a>

        <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample08">
        
        </div>
            <div class="cart1">
            <button class="card-iteam btn card1" type="button">
                <i class="fas fa-cart-arrow-down"></i> <span>0 sp - </span> <span> 0 VNĐ</span>
            </button>
        </div>
    </div>
</nav>
<!-- end nav -->
<section id="loading-gif">
    <div class="load-gif2">
        <img src="resources/img/Spin-1.3s-200px.gif" alt="">
    </div>
</section>