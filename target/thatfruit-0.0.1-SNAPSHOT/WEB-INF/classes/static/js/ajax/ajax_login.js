$(document).ready(function () {
    checkLogin();

    $("#submit").click(() => {
        checkUserLogin();
    })

});

// lấy dữ liệu từ form đăng nhập của người dùng gửi lên database
function postUserLogin(user) {

    $.ajax({
        type: "POST",
        url: URL_API + "/v1/public/user/login",
        data: JSON.stringify(user),
        headers: {
            "adminbksoftwarevn": tokenHeader_value
        },
        contentType: "application/json",
        timeout: 2000,
        success: function (user) {
            if (user == "username or password is not correct") {
                alterThatFruitWarning("Tài khoản hoặc mật khẩu không chính xác", 1000);
            } else {
                localStorage.setItem("infoUserLogin", JSON.stringify(user));
                token = localStorage.getItem("infoUserLogin");
                document.cookie = "token=" + token;
                document.cookie = "dataInfo=" + user.id;
                window.location = "home";
            }

        },
        error: function (err) {
            alterThatFruitDanger("Sai tài khoản hoặc mật khẩu, xin vui lòng kiểm tra lại", 1000);
        }
    });
}

// check name và passwword đứng định dạng mới cho nhậm
function checkUserLogin() {
    $(".form-group >input").map(function () {
        if ($(this).val() == "") {
            $(this).siblings().children(".error-log").css({"display": "inline"})
        } else {
            $(this).siblings().children(".error-log").css({"display": "none"});
            if ($("#username").val().match("(^[a-z][a-z0-9_\\.]{5,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$)")) {
                let user = {
                    'username': $("#username").val(),
                    'password': $("#password").val()
                }
                postUserLogin(user);
            } else {
                alterThatFruitWarning(" Email sai định dạng vui lòng kiểm tra lại tên email", 1000);
            }
        }
    })


}

