$(document).ready(function () {
    findIdNew();
    getAllNewsForNewDeatail();
    getNewDetailProducts();
    getNewDetailTopic();
    findAllSmallCategory();
    searchNews();
});

function setNewDetailProducts(products) {
    var text = `<span class="ndrelatedp-cap">Sản phẩm liên quan</span>`;
    products.map((data, index) => {
        text += `
        <div class="ndrelatedp-wp" id="_${index}">
        <div class="ndrwp-img">
           <img src="${data.image}" alt="">
            <div class="ndrwpi-mb">
            <a href="../product?id=${data.id}"><i class="fas fa-eye"></i></a>
            </div>
        </div>
        <div class="ndrwp-text">
            <a class="ndrwpt-cap" href="../product?id=${data.id}">${
            data.name
            }</a>
            <i class="ndr-price">${data.saleCostWholesale} đ</i>
            <div class="rating">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
            </div>
        </div>
    </div> `;
    });
    $(".ndrelatedp").html(text);
    $("#_3").css("border", "none");
}

function getNewDetailProducts() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: URL_API + `/v1/public/product/page?page=2&size=4`,
        headers: {
            adminbksoftwarevn: tokenHeader_value
        },
        timeout: 30000,
        success: function (result) {
            setNewDetailProducts(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function setNewDetailTopic(news) {
    var text = `<span class="ndtags-cap">Search by Tags</span>`;
    news.map(data => {
        text += `
      <a href="../news?id=${data.id}">${data.name}</a>`;
    });
    $(".ndtags").html(text);
}

function getNewDetailTopic() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: URL_API + `/v1/public/topic/all`,
        headers: {
            adminbksoftwarevn: tokenHeader_value
        },
        timeout: 30000,
        success: function (result) {
            setNewDetailTopic(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function setNewsDeatailMenu(smalcategory) {
    var text = '';
    smalcategory.map((data, index) => {
        text += `
        <li><a href="list-product?small-category=${data.id}">${data.name}</a> <span>()</span></li>`;
        findAllSizeSmallCategoryById(data.id, index + 1);
    });
    $(".ndplist-list").html(text);
}

function findAllSmallCategory() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: URL_API + "/v1/public/small-category/all",
        headers: {
            adminbksoftwarevn: tokenHeader_value
        },
        success: function (result) {
            setNewsDeatailMenu(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);

        }
    });
}

var findAllSizeSmallCategoryById = async (id, index) => {
    let data;
    await $.ajax({
        type: "GET",
        url: URL_API + "/v1/public/product/by-small-category/page?id-small=" + id,
        dataType: "json",
        headers: {
            adminbksoftwarevn: tokenHeader_value
        },
        timeout: 30000,
        success: function (result) {
            data = result;
            $(`.ndplist-list li:nth-child(${index}) span`).text(`(${data.length})`);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            errMess(jqXHR, textStatus, errorThrown);
        }
    });
    return data;
};


// =====================Search News=========================


function searchNews() {
    $('#keysearchnews').keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            var valName = $('#keysearchnews').val();

            if (valName == '') {
               alterThatFruitWarning("Nhập tin bạn muốn tìm kiếm !")
            } else {
                window.location.href = '../news?title=' + valName;
            }
        }
    });
    $("#ndsearchbt").click(function () {
        var valName = $('#keysearchnews').val();

        if (valName == '') {
           alterThatFruitWarning("Nhập tin bạn muốn tìm kiếm !")
        } else {
            window.location.href = '../news?title=' + valName;
        }
    });
}


// ====================== NewDetailArc ======================


function getNewDetailArc(id) {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: URL_API + `/v1/public/news/find-by-id?id=` + id,
        headers: {
            adminbksoftwarevn: tokenHeader_value
        },
        timeout: 30000,
        success: function (result) {

            setNewsDeatailArc(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function setNewsDeatailArc(news) {
    var list = (news.time + "").split(",");
    if (list[4] < 10) {
        list[4] = "0" + list[4];
    }
    var timePost = list[2] + "-" + list[1] + "-" + list[0] + " " + list[3] + ":" + list[4];
    var pre;
    var next;
    switch(news.id) {
        case 1:
            next = news.id +1;
            pre = numOfNew;
            break;
        case numOfNew:
            next = 1;
            pre = news.id -1;
            break;
        default:
            pre = news.id - 1;
            next = news.id +1;
    }
    text = `<div class="artd-wp">
      <span class="d-block ar-cap ">${news.title}</span>
      <span> Danh mục: <a href="#"> ${news.topic.name} </a></span> //
      <span> Ngày: <time> ${timePost}</time></span>
      </div>
      <div class="arccontent" style="font-size: 14px; color: #8d8d8d; font-weight: 600;"> ${news.content}</div>
      <div class="arcshare-wp">
      <div class="arc-tags">
         <span> Tag:</span> <a>${news.topic.name}</a>
      </div>
      <div class="arc-share">
         <span> Share:</span> 
          <a href="https://www.facebook.com/sharer/sharer.php?u=${window.location.href}" id="sharefb"><i class="fab fa-facebook-f"></i></a>
          <a href="https://twitter.com/share?text=${news.title}&url=${window.location.href}" id="sharetw"><i class="fab fa-twitter"></i></a>
          <a href="https://plus.google.com/share?url=${window.location.href}" id="shareg+"><i class="fab fa-google-plus-g"></i></a>
      </div>
      
       </div>
      <div class="arc-np">
        <a href="../newdetail?id=${pre}"><i class="fas fa-long-arrow-alt-left"></i>Trước</a>
        <a href="../newdetail?id=${next}">Sau <i class="fas fa-long-arrow-alt-right"></i></a>
      </div>
      </div>`;
    $(".ndarticle").html(text);
}
var numOfNew;
function getAllNewsForNewDeatail() {
    $.ajax({
        type: "GET",
        headers: {
            adminbksoftwarevn: tokenHeader_value,
        },
        url: URL_API + "/v1/public/record/all",
        success: function (records) {
            const listSize = Object.keys(records).length;
            if (listSize > 0) {
                records.map(function (record) {
                    if (record.name === 'news') {
                        numOfNew = record.number;
                    }
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // errMess(jqXHR, textStatus, errorThrown);
        }
    });
}
function findIdNew() {
    const urlNewsById = window.location.href;

    var str = urlNewsById.split("id=");
    const id = str[str.length - 1];

    if ((id - 1) >= 0) {
        getNewDetailArc(id)
        // findAllPageNewsNumberByTopicId(id);
    } else {
        getNewDetailArc(1)
    }
}



