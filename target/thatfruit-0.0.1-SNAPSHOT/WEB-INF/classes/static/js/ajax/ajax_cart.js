$(document).ready(function () {
    checkLogin();
    getInfoProductOfLocal();
    getInfoUserLocal();
    upDateInfoUser();
});
// get info user at local
function getInfoUserLocal() {
    let user = JSON.parse(localStorage.getItem("infoUser"));
    let tmp = "";
    if (user === null) {
        alterThatFruitWarning("Thông tin bạn chưa có vui lòng nhập thông tin để sử dụng chức năng này");

    } else {
        tmp += `
                        <div class="usif-a">
                           
                            <span>Thông Tin Khách Hàng</span>
                        </div>
                        <div class="usif-if">
                            <div class="us2if-wp">
                                <span>Tên</span>
                                <input id="name" type="text" value="${user.fullName}" required>
                            </div>
                            <div class="us2if-wp">
                                <span>Email</span>
                                <input id="email" type="text"value="${user.email}" pattern="^[a-z][a-z0-9_\\.]{5,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$" title="Vui lòng nhập đúng email" required>
                            </div>
                            <div class="us2if-wp">
                                <span>Số điện thoại</span>
                                <input id="phoneNumber" pattern="(09|01[2|6|8|9])+([0-9]{8})\\b" title="Vui lòng nhập đúng số điện thoại" type="text"value="" required>
                            </div>
                            <div class="us2if-wp">
                                <span>Địa chỉ</span>
                                <input id="address" type="text" value="" required>
                            </div>

                            <div class="us2if-wp">
                                <span>Yêu cầu đối với nhà cung cấp</span>
                                <input id="request" type="text" >
                            </div>
                            
                        </div>
                       
        `;
        $("#input-info-user-cart").html(tmp);
    }
}

// get info user at local
function getInfoUserLocal() {
    let user = JSON.parse(localStorage.getItem("infoUser"));
    let tmp = "";
    if (user === null) {
        alterThatFruitWarning("Thông tin bạn chưa có vui lòng nhập thông tin để sử dụng chức năng này");

    } else {
        tmp += `
                        <div class="usif-a">
                           
                            <span>Thông Tin Khách Hàng</span>
                        </div>
                        <div class="usif-if">
                            <div class="us2if-wp">
                                <span>Tên</span>
                                <input id="name" type="text" value="${user.fullName}" required>
                            </div>
                            <div class="us2if-wp">
                                <span>Email</span>
                                <input id="email" type="text"value="${user.email}" pattern="^[a-z][a-z0-9_\\.]{5,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$" title="Vui lòng nhập đúng email" required>
                            </div>
                            <div class="us2if-wp">
                                <span>Số điện thoại</span>
                                <input id="phoneNumber" pattern="(09|01[2|6|8|9])+([0-9]{8})\\b" title="Vui lòng nhập đúng số điện thoại" type="text"value="" required>
                            </div>
                            <div class="us2if-wp">
                                <span>Địa chỉ</span>
                                <input id="address" type="text" value="" required>
                            </div>

                            <div class="us2if-wp">
                                <span>Yêu cầu đối với nhà cung cấp</span>
                                <input id="request" type="text" >
                            </div>
                            
                        </div>
                       
        `;
        $("#input-info-user-cart").html(tmp);
    }
}

// lay san pham tu local
function getInfoProductOfLocal() {
    let moneyItem=0;
    let tmp = "";
    let listPrd = JSON.parse(localStorage.getItem("productAddToCard"));
    if (listPrd != null) {
        listPrd.map((prd, index) => {
            if (prd.saleCostRetail !== 0 || prd.saleCostRetail !== null){
                moneyItem = prd.quantity* prd.saleCostRetail;
            }else {
                moneyItem = prd.quantity* prd.originCostRetail;
            }
            tmp += `
       <tr class="id-pr-cart" data-idPdCart=${prd.id}>
                                <td scope="row"><i class="fas fa-times click-remove-pr"  ></i></td>
                                <td><img src="${prd.image}" alt=""></td>
                                <td class="ctpc">${prd.name}</td>
                                <td>${formatNumber(prd.saleCostRetail, '.', '.')}</td>
                                <td>
                                    <div class="cidoa">
                                        <i class="fas fa-arrow-alt-circle-down click-increase"></i>
                                        <input type="text" class="ip-cart" name="" id="" value="${prd.quantity}" placeholder="1">
                                        <i class="fas fa-arrow-alt-circle-up click-reduction"></i>
                                    </div>
                                </td>
                                <td>${formatNumber(moneyItem, '.', '.')} đ</td>
                            </tr>
                            `
        });

        $("#tb-cart").html(tmp)
        clickRemoveProduct();
        changeAmountProduct();
        changeInputAmountProduct();
        upDatePrice();
    } else {
        alterThatFruitWarning("Vui lòng thêm sản phẩm vào giỏ để sử dụng chức năng này ", 1000)
    }

}

// click xoa san pham
function clickRemoveProduct() {
    $('.click-remove-pr').click(function () {
        listPrd = JSON.parse(localStorage.getItem("productAddToCard"));
        var findId = $(this).parents(".id-pr-cart").attr("data-idPdCart"); //lây ra id prduct
        let list = listPrd.filter(pr => pr.id != findId);

        localStorage.setItem("productAddToCard", JSON.stringify(list));
        if (list.length == 0 || list === null) {
            $("#tb-cart").html("");

            updateAmountProductInCart()
        } else {
            upDateProduct(list);
            // upDatePrice();
            // updateAmountProductInCart();
        }
        upDatePrice();
        // else if (list.length == 1) {
        //         $("#tb-cart").html("");
        //         upDatePrice();
        //         // updateAmountProductInCart()
        //     }
    })
}

// cap nhat lai san pham sau moi lan xóa
function upDateProduct(list) {
    let moneyItem=0;
    let tmp = "";
    list.map(function (prd, index) {
        if (prd.saleCostRetail !== 0 || prd.saleCostRetail !== null){
            moneyItem = prd.quantity* prd.saleCostRetail;
        }else {
            moneyItem = prd.quantity* prd.originCostRetail;
        }
        tmp += `
       <tr class="id-pr-cart" data-idPdCart=${prd.id}>
                                <td scope="row"><i class="fas fa-times click-remove-pr" ></i></td>
                                <td><img src="${prd.image}" alt=""></td>
                                <td class="ctpc">${prd.name}</td>
                                <td>${formatNumber(prd.saleCostRetail, '.', '.')}</td>
                                <td>
                                    <div class="cidoa">
                                        <i class="fas fa-arrow-alt-circle-down click-increase" ></i>
                                        <input type="text" class="ip-cart" name="" id="" value="${prd.quantity}" placeholder="1">
                                        <i class="fas fa-arrow-alt-circle-up click-reduction"></i>
                                    </div>
                                </td>
                                <td>${formatNumber(moneyItem, '.', '.')} đ</td>
                            </tr>
        `;
    })
    $("#tb-cart").html(tmp);
    clickRemoveProduct();
    changeAmountProduct();
    changeInputAmountProduct();
    upDatePrice();
}

// thay đổi số lượng sản phẩm
function changeAmountProduct() {

    // click giam so san pham
    $(".click-increase").click(function () {

        let listPrd = JSON.parse(localStorage.getItem("productAddToCard"));
        let id = $(this).parents(".id-pr-cart").attr("data-idPdCart");
        if (listPrd == null || listPrd.length == 0) {
        } else {
            listPrd.map(function (data, index) {
                if (data.id == id) {
                    if (data.quantity <= 0) {
                        alterThatFruitWarning("Số lượng sản phẩm không được nhỏ hơn 0", 1000)
                    } else {
                        data.quantity = data.quantity - 1;


                    }
                }

            })
        }


        localStorage.setItem("productAddToCard", JSON.stringify(listPrd));
        upDateProduct(listPrd);
        changeInputAmountProduct();
        upDatePrice();
        updateAmountProductInCart();
    })
    // click tang so san pham
    $(".click-reduction").click(function () {

        let listPrd = JSON.parse(localStorage.getItem("productAddToCard"));
        let id = $(this).parents(".id-pr-cart").attr("data-idPdCart");
        if (listPrd == null || listPrd.length == 0) {

        } else {
            listPrd.map(function (data, index) {

                if (data.id == id) {
                    if (data.quantity <= 100) {
                        data.quantity = data.quantity + 1;


                    } else {
                        alterThatFruitWarning("Số lượng sản phẩm quá lớn vui lòng liên hệ với nhà cung cấp")

                    }
                }
            });
        }
        localStorage.setItem("productAddToCard", JSON.stringify(listPrd));
        upDateProduct(listPrd);
        changeInputAmountProduct();
        upDatePrice();
        updateAmountProductInCart();
    })
}

//ham tinh gia tien
function upDatePrice() {
    var money = 0;
    var sumQuantity = 0;
    var sumMoney = 0;
    let listPrd = JSON.parse(localStorage.getItem("productAddToCard"));
    if (listPrd == null || listPrd.length == 0) {
        money = 0;
        sumMoney = 0;
    } else {
        // tinh tien
        listPrd.map(function (data, index) {
            sumQuantity += data.quantity;
            let price = 0;
            if (data.saleCostRetail == null || data.saleCostRetail == 0) {
                if (data.originCostRetail > 0 || data.originCostRetail != null) {
                    price = data.originCostRetail * data.quantity;

                }
            } else {
                price = data.saleCostRetail * data.quantity;
            }
            money += price;
        })
        sumMoney = money + 50000;
    }

    // get data money
    let tmp = "";

    tmp += `
          <tr>
                                    <th scope="row"> Tổng Sản phẩm</th>
                                    <td>${formatNumber(money, '.', '.')}đ</td>
                                </tr>
                                <tr>
                                    <th scope="row"> Phí vận chuyển</th>
                                    <td>50.000 đ</td>
                                </tr>
                                <tr>
                                    <th scope="row"> Tổng tiền</th>
                                    <td id="sumMoney">${formatNumber(sumMoney, '.', '.')} đ</td>
                                </tr>
        `;
    $("#table-money").html(tmp);
    localStorage.setItem("money", sumMoney);

}

// thay doi ở ô input so luong san pham
function changeInputAmountProduct() {
    $(".ip-cart").change(function () {
        let listPrd = JSON.parse(localStorage.getItem("productAddToCard"));
        let id = $(this).parents(".id-pr-cart").attr("data-idPdCart");
        let value = $(this).val();

        if (value <= 100 && value >= 0) {
            listPrd.map(function (data, index) {
                if (data.id == id) {
                    data.quantity = value;
                }
            })
            localStorage.setItem("productAddToCard", JSON.stringify(listPrd));
        } else if (value < 0) {
            $(this).val(0);
            alterThatFruitWarning("Số lượng sản phẩm không được nhỏ hơn 0.")
        } else {
            $(this).val(100);
            alterThatFruitWarning("Số lượng sản phẩm quá lớn. Vui lòng liên hệ với nhà cung cấp để được hỗ trợ ")
        }
        upDatePrice();
    })

}


// cập nhật lại thông tin khách hàng và post buyform
function upDateInfoUser() {
    $("#submit-cart").click(function () {

        var userId = -1;
        let user = JSON.parse(localStorage.getItem("infoUser"));
        let listPrd = JSON.parse(localStorage.getItem("productAddToCard"));
        if (user != null) {
            userId = user.id;
        }
        if (($("#name").val() && $("#email").val() && $("#address").val() && $("#phoneNumber").val()) === "" || ($("#name").val() && $("#email").val() && $("#address").val() && $("#phoneNumber").val()) === null) {
            alterThatFruitWarning("Bạn chưa có thông tin. Vui lòng điền đầy đủ thông tin để đặt hàng", 1000);
        } else {
            let phone = $("#phoneNumber").val().substr(1) - 0;
            let buyForm = {
                "name": $("#name").val(),
                "phone": phone,
                "email": $("#email").val(),
                "address": $("#address").val(),
                "note": $("#request").val(),
                "totalPrice": Number(localStorage.getItem("money"))
            };
            let updateInfo = {
                "id": userId,
                "fullName": $("#name").val(),
                "phone": $("#phoneNumber").val(),
                "email": $("#email").val(),
                "address": $("#address").val(),
            };

            if (listPrd !== null && listPrd.length != 0) {
                listCart = listPrd.map((data, index) => {
                    return {
                        "productId": data.id,
                        "quantity": data.quantity
                    }
                });

                viewLoadingGif();
                $.ajax({
                    type: "POST",
                    url: URL_API + "/v1/public/buy-form/add-buy-form?user-id=" + userId,
                    data: JSON.stringify(buyForm),
                    headers: {
                        "adminbksoftwarevn": tokenHeader_value
                    },
                    contentType: "application/json",
                    timeout: 2000,
                    success: function (data) {
                        localStorage.setItem("infoUser", JSON.stringify(updateInfo));
                        getInfoUserLocal();
                        alterThatFruitSuccess("Bạn đã đặt hàng thành công", 3000);
                        $.ajax({
                            type: "POST",
                            url: URL_API + "/v1/public/buy-form/add-products?buy-form-id=" + data.id,
                            headers: {
                                "adminbksoftwarevn": tokenHeader_value
                            },
                            async: false,
                            data: JSON.stringify(listCart),
                            contentType: "application/json",
                            timeout: 2000,
                            success: function (data2) {

                                localStorage.removeItem("productAddToCard");
                                localStorage.removeItem("money");
                                upDatePrice();
                                updateAmountProductInCart();
                                hideLoadingGif();
                                $("#tb-cart").html("");
                            }, error: function (err) {
                                console.log(err);

                            }
                        })
                        // window.location = "user";
                    },
                    error: function (err) {
                        console.log(err);

                    }
                });
            } else {
                alterThatFruitWarning("Vui lòng chọn sản phẩm", 1000);
            }
        }

    });
}